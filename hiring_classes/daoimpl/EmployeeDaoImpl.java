package net.thumbtack.school.hiring.daoimpl;

import net.thumbtack.school.hiring.dao.EmployeeDao;
import net.thumbtack.school.hiring.database.DataBase;
import net.thumbtack.school.hiring.dto.request.employee.ChangeEmployeeDataDtoRequest;
import net.thumbtack.school.hiring.dto.request.skill.ChangeSkillLevelDtoRequest;
import net.thumbtack.school.hiring.dto.response.modelType.SkillDtoResponse;
import net.thumbtack.school.hiring.errors.ErrorCode;
import net.thumbtack.school.hiring.errors.ServerException;
import net.thumbtack.school.hiring.model.Employee;
import net.thumbtack.school.hiring.model.Skill;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class EmployeeDaoImpl implements EmployeeDao {
    @Override
    public Employee getEmployee(String token) throws ServerException {
        try{
            return (Employee) DataBase.getLogInUser(token);
        }catch (ServerException ex){
            throw new ServerException(ErrorCode.EMPLOYEE_DONT_FOUND);
        }

    }

    @Override
    public List<Employee> getAllEmployees() {
        return DataBase.getAllEmployee();
    }

    @Override
    public Employee getRegisterEmployee(String login, String password) throws ServerException {
        try{
            return (Employee) DataBase.getOneUserOfAll(login, password);
        }catch (ServerException ex){
            throw new ServerException(ErrorCode.EMPLOYEE_DONT_FOUND);
        }
    }

    @Override
    public void addEmployeeInSortedTree(Employee employee, Skill skill) {
        DataBase.addEmployeeInSortedTree(employee, skill);
    }

    @Override
    public Set<Employee> getEmployeeSetBySkillLevel(String skillName, int minLevel) {
        Set<Employee> result = new HashSet<>();
        Skill skill = new Skill(skillName, minLevel);
        for (int i = minLevel + 1; i < 7; i++) {
            result.addAll(DataBase.getEmployeeSetBySkill(skill));
            skill.setLevel(i);
        }
        return result;
    }

    public void removeEmployeeSkillFromTree(SkillDtoResponse skillByName, Employee employee) {
        DataBase.getEmployeeSetBySkill(new Skill(skillByName)).remove(employee);
    }

    @Override
    public void clearSkillDataBase() {

    }

    @Override
    public void clearEmployeeDataBase() {

    }

    @Override
    public void changeEmployeeData(ChangeEmployeeDataDtoRequest employeeData) throws ServerException {
        Employee em = getEmployee(employeeData.getToken());
        em.setFirstName(employeeData.getName());
        em.setEmail(employeeData.getEmail());
        em.setPassword(employeeData.getPassword());
    }

    @Override
    public boolean employeeIsActive(String token) throws ServerException {
        return getEmployee(token).isActive();
    }

    @Override
    public void madeAccountInactive(String userTokenDtoJson) throws ServerException {
        getEmployee(userTokenDtoJson).setActive(false);
    }

    @Override
    public void madeAccountActive(String token) throws ServerException {
        getEmployee(token).setActive(true);
    }

    @Override
    public void addNewSkill(Skill skill, String token) throws ServerException {
        Employee employee = getEmployee(token);
        employee.addNewSkill(new SkillDtoResponse(skill.getName(), skill.getLevel()));
        addEmployeeInSortedTree(employee, skill);
    }

    @Override
    public boolean containsSkill(String skillName, String token) throws ServerException {
        for (SkillDtoResponse s : getEmployee(token).getSkillsList()) {
            if (skillName.compareTo(s.getName()) == 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void removeSkill(String token, String skillName) throws ServerException {

        Employee employee = getEmployee(token);
        removeEmployeeSkillFromTree(employee.getSkillByName(skillName), employee);
        employee.removeSkill(skillName);
    }

    @Override
    public void changeSkillLevel(ChangeSkillLevelDtoRequest skillData) throws ServerException {
        getEmployee(skillData.getToken()).changeSkillLevel(skillData.getSkillName(), skillData.getNewLevel());
    }

    @Override
    public int getSkillLevel(String token, String skillName) throws ServerException {
        return getEmployee(token).getSkillByName(skillName).getLevel();
    }
}
