package net.thumbtack.school.hiring.daoimpl;

import net.thumbtack.school.hiring.dao.UserDao;
import net.thumbtack.school.hiring.database.DataBase;
import net.thumbtack.school.hiring.errors.ServerException;
import net.thumbtack.school.hiring.model.User;

public class UserDaoImpl implements UserDao {
    @Override
    public void logIn(String token, User user) throws ServerException {
        DataBase.userLogIn(token, user);
    }

    @Override
    public void logOut(String token) throws ServerException {
        DataBase.userLogOut(token);
    }

    @Override
    public User getUser(String login, String password) throws ServerException {
        return DataBase.getOneUserOfAll(login, password);
    }

    @Override
    public void registerUser(User user) throws ServerException {
        DataBase.registerUser(user);
    }

    @Override
    public void removeUser(String token) throws ServerException {
        DataBase.removeUser(token);
    }

    @Override
    public boolean isLogIn(String userToken) {
        return DataBase.userIsLogIn(userToken);
    }

}
