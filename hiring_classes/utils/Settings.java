package net.thumbtack.school.hiring.utils;

import net.thumbtack.school.hiring.dao.EmployeeDao;
import net.thumbtack.school.hiring.dao.EmployerDao;
import net.thumbtack.school.hiring.dao.UserDao;
import net.thumbtack.school.hiring.daoimpl.EmployeeDaoImpl;
import net.thumbtack.school.hiring.daoimpl.EmployerDaoImpl;
import net.thumbtack.school.hiring.daoimpl.UserDaoImpl;

public class Settings {
    private final EmployeeDao employeeDao;
    private final EmployerDao employerDao;
    private final UserDao userDao;

    public Settings(Mode mode) {
        if (mode == Mode.RAM) {
            this.employeeDao = new EmployeeDaoImpl();
            this.employerDao = new EmployerDaoImpl();
            this.userDao = new UserDaoImpl();
        } else if (mode == Mode.SQL) {
            this.employeeDao = new net.thumbtack.school.hiring.mybatis.daoimpl.EmployeeDaoImpl();
            this.employerDao = new net.thumbtack.school.hiring.mybatis.daoimpl.EmployerDaoImpl();
            this.userDao = new net.thumbtack.school.hiring.mybatis.daoimpl.UserDaoImpl();
        } else {
            this.employeeDao = null;
            this.employerDao = null;
            this.userDao = null;
        }
    }

    public EmployeeDao getEmployeeDao() {
        return employeeDao;
    }

    public EmployerDao getEmployerDao() {
        return employerDao;
    }

    public UserDao getUserDao() {
        return userDao;
    }
}
