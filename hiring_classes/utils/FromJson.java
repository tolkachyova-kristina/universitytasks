package net.thumbtack.school.hiring.utils;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import net.thumbtack.school.hiring.errors.ErrorCode;
import net.thumbtack.school.hiring.errors.ServerException;

import java.lang.reflect.Type;

public class FromJson {
    private final static Gson GSON = new Gson();

    public static <T> T getClassFromJson(String json, Type neededClass) throws ServerException {
        if (json == null) {
            throw new ServerException(ErrorCode.NULL_INPUT_STRING);
        }
        try {
            T dto = GSON.fromJson(json, neededClass);
            return dto;
        } catch (JsonSyntaxException e) {
            throw new ServerException(ErrorCode.WRONG_INPUT_DATA);
        }
    }
}
