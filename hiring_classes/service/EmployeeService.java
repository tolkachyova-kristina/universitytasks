package net.thumbtack.school.hiring.service;

import com.google.gson.Gson;
import net.thumbtack.school.hiring.dao.EmployeeDao;
import net.thumbtack.school.hiring.dao.EmployerDao;
import net.thumbtack.school.hiring.dao.UserDao;
import net.thumbtack.school.hiring.dto.request.employee.ChangeEmployeeDataDtoRequest;
import net.thumbtack.school.hiring.dto.request.employee.RegisterEmployeeDtoRequest;
import net.thumbtack.school.hiring.dto.request.user.TokenDtoRequest;
import net.thumbtack.school.hiring.dto.request.vacancy.DemandDtoRequest;
import net.thumbtack.school.hiring.dto.response.baseType.BooleanDtoResponse;
import net.thumbtack.school.hiring.dto.response.baseType.EmptyDtoResponse;
import net.thumbtack.school.hiring.dto.response.modelType.EmployeeDataDtoResponse;
import net.thumbtack.school.hiring.dto.response.modelType.SkillDtoResponse;
import net.thumbtack.school.hiring.dto.response.serviceType.ErrorDtoResponse;
import net.thumbtack.school.hiring.errors.ErrorCode;
import net.thumbtack.school.hiring.errors.ServerException;
import net.thumbtack.school.hiring.model.Employee;
import net.thumbtack.school.hiring.model.Vacancy;
import net.thumbtack.school.hiring.utils.FromJson;
import net.thumbtack.school.hiring.utils.Mode;
import net.thumbtack.school.hiring.utils.Settings;

import java.util.*;
import java.util.stream.Collectors;

public class EmployeeService {
    private final EmployeeDao employeeDao;
    private final EmployerDao employerDao;
    private final UserDao userDao;
    private final Gson gson = new Gson();

    public EmployeeService(Mode mode) {
        Settings set = new Settings(mode);
        employeeDao = set.getEmployeeDao();
        employerDao = set.getEmployerDao();
        userDao = set.getUserDao();
    }

    private static void validateTokenDto(TokenDtoRequest userTokenDtoJson) throws ServerException {
        if (userTokenDtoJson == null) {
            throw new ServerException(ErrorCode.WRONG_INPUT_DATA);
        }
        if (userTokenDtoJson.getToken() == null || "".compareTo(userTokenDtoJson.getToken()) == 0) {
            throw new ServerException(ErrorCode.WRONG_TOKEN);
        }
    }

    public String registerEmployee(String json) {
        try {
            RegisterEmployeeDtoRequest registerData = FromJson.getClassFromJson(json, RegisterEmployeeDtoRequest.class);
            validateRegisterEmployeeDtoRequest(registerData);
            Employee employee = new Employee(registerData);
            userDao.registerUser(employee);
            String token = UUID.randomUUID().toString();
            userDao.logIn(token, employee);
            return gson.toJson(new TokenDtoRequest(token));
        } catch (ServerException e) {
            return gson.toJson(new ErrorDtoResponse(e.getErrorCode()));
        }
    }

    public String changeEmployeeData(String newEmployeeDataDtoJson) {
        try {
            ChangeEmployeeDataDtoRequest employeeData = FromJson.getClassFromJson(newEmployeeDataDtoJson, ChangeEmployeeDataDtoRequest.class);
            validateChangeEmployeeDataDto(employeeData);
            employeeDao.changeEmployeeData(employeeData);
            return gson.toJson(new EmptyDtoResponse());
        } catch (ServerException e) {
            return gson.toJson(new ErrorDtoResponse(e.getErrorCode()));
        }
    }

    public String madeAccountInactive(String userTokenDtoJson) {
        try {
            TokenDtoRequest tokenDto = FromJson.getClassFromJson(userTokenDtoJson, TokenDtoRequest.class);
            validateTokenDto(tokenDto);
            employeeDao.madeAccountInactive(tokenDto.getToken());
            return gson.toJson(new EmptyDtoResponse());
        } catch (ServerException e) {
            return gson.toJson(new ErrorDtoResponse(e.getErrorCode()));
        }
    }

    public String madeAccountActive(String userTokenDtoJson) {
        try {
            TokenDtoRequest tokenDto = FromJson.getClassFromJson(userTokenDtoJson, TokenDtoRequest.class);
            validateTokenDto(tokenDto);
            employeeDao.madeAccountActive(tokenDto.getToken());

            return gson.toJson(new EmptyDtoResponse());
        } catch (ServerException e) {
            return gson.toJson(new ErrorDtoResponse(e.getErrorCode()));
        }
    }

    public String getEmployeeData(String employeeTokenGson) {
        try {
            TokenDtoRequest tokenDto = FromJson.getClassFromJson(employeeTokenGson, TokenDtoRequest.class);
            validateTokenDto(tokenDto);
            Employee em = employeeDao.getEmployee(tokenDto.getToken());
            Gson gson = new Gson();
            return gson.toJson(new EmployeeDataDtoResponse(em.getFirstName(), em.getEmail(), em.getSkillsList()));
        } catch (ServerException e) {
            return gson.toJson(new ErrorDtoResponse(e.getErrorCode()));
        }
    }

    public String containsEmployee(String employeeGson) {
        try {
            RegisterEmployeeDtoRequest registerData = FromJson.getClassFromJson(employeeGson, RegisterEmployeeDtoRequest.class);
            validateRegisterEmployeeDtoRequest(registerData);
            Employee employee = new Employee(registerData);
            Employee em = new Employee(employeeDao.getRegisterEmployee(employee.getLogin(), employee.getPassword()));
            boolean t = em.equals(employee);
            return gson.toJson(new BooleanDtoResponse(t));
        } catch (ServerException e) {
            return gson.toJson(new ErrorDtoResponse(e.getErrorCode()));
        }
    }

    // ======================================================== //

    //получить список всех вакансий соответствует требованиям работодателя на необходимом уровне
    //все умения соответсвующего уровня

    public String employeeIsActive(String employeeTokenGson) {
        try {
            TokenDtoRequest tokenDto = FromJson.getClassFromJson(employeeTokenGson, TokenDtoRequest.class);
            validateTokenDto(tokenDto);
            return gson.toJson(new BooleanDtoResponse(employeeDao.employeeIsActive(tokenDto.getToken())));
        } catch (ServerException e) {
            return gson.toJson(new ErrorDtoResponse(e.getErrorCode()));
        }
    }

    public String getListOfAllVacanciesMeetsTheRequirementsOfTheEmployerAtANeededLvl(String employeeTokenGson) {
        List<Vacancy> finish = new ArrayList<>();
        try {
            TokenDtoRequest tokenDto = FromJson.getClassFromJson(employeeTokenGson, TokenDtoRequest.class);
            validateTokenDto(tokenDto);
            Employee employee = new Employee(employeeDao.getEmployee(tokenDto.getToken()));
            List<SkillDtoResponse> skills = employee.getSkillsList();
            List<String> skillsName = skills.stream().map(x -> x.getName()).collect(Collectors.toList());

            Set<Vacancy> vacancySet = new HashSet<>();
            for (SkillDtoResponse skill : skills) {
                vacancySet.addAll(employerDao.getVacancySetBySkillLevel(skill.getName(), skill.getLevel()));
            }

            for (Vacancy v : vacancySet) {
                boolean key = true;
                List<String> vacancySkillsName = v.getAllSkills().stream().map(x -> x.getName()).collect(Collectors.toList());
                if (v.getActive() && skillsName.containsAll(vacancySkillsName)) {
                    for (SkillDtoResponse s : v.getAllSkills()) {
                        if (s.getLevel() > employee.getSkillByName(s.getName()).getLevel()) {
                            key = false;
                        }
                    }
                } else {
                    key = false;
                }
                if (key) {
                    finish.add(v);
                }
            }
            finish.sort((o1, o2) -> o1.getPostName().compareTo(o2.getPostName()));
            Gson gson = new Gson();
            return gson.toJson(finish);
        } catch (ServerException e) {
            return gson.toJson(new ErrorDtoResponse(e.getErrorCode()));
        }

    }

    //только необходимые навыки на нужном уровне, остальные на любом
    public String getListOfAllVacanciesMeetsTheBindingRequirementsOfTheEmployerAtANeededLvl(String employeeTokenGson) {
        List<Vacancy> finish = new ArrayList<>();
        try {
            TokenDtoRequest tokenDto = FromJson.getClassFromJson(employeeTokenGson, TokenDtoRequest.class);
            validateTokenDto(tokenDto);
            Employee employee = employeeDao.getEmployee(tokenDto.getToken());
            List<SkillDtoResponse> skills = employee.getSkillsList();
            List<String> skillsName = skills.stream().map(x -> x.getName()).collect(Collectors.toList());

            Set<Vacancy> vacancySet = new HashSet<>();
            for (SkillDtoResponse skill : skills) {
                vacancySet.addAll(employerDao.getVacancySetBySkillLevel(skill.getName(), 1));
            }
            for (Vacancy v : vacancySet) {
                boolean key = true;
                List<String> vacancySkillsName = v.getAllSkills().stream().map(x -> x.getName()).collect(Collectors.toList());
                if (v.getActive() && skillsName.containsAll(vacancySkillsName)) {
                    for (DemandDtoRequest d : v.getDemands()) {
                        if (d.getSkill().getLevel() > employee.getSkillByName(d.getSkill().getName()).getLevel() && d.isMandatory()) {
                            key = false;
                        }
                    }
                } else {
                    key = false;
                }
                if (key) {
                    finish.add(v);
                }
            }
            finish.sort((o1, o2) -> o1.getPostName().compareTo(o2.getPostName()));
            Gson gson = new Gson();
            return gson.toJson(finish);
        } catch (ServerException e) {
            return gson.toJson(new ErrorDtoResponse(e.getErrorCode()));
        }

    }

    public String getListOfAllVacanciesMeetsTheRequirementsOfTheEmployerOnAnyLvl(String employeeTokenDto) {
        List<Vacancy> finish = new ArrayList<>();
        try {
            TokenDtoRequest tokenDto = FromJson.getClassFromJson(employeeTokenDto, TokenDtoRequest.class);
            validateTokenDto(tokenDto);
            Employee employee = employeeDao.getEmployee(tokenDto.getToken());
            List<String> skillsName = employee.getSkillsList().stream().map(x -> x.getName()).collect(Collectors.toList());

            Set<Vacancy> vacancySet = new HashSet<>();
            for (String skill : skillsName) {
                vacancySet.addAll(employerDao.getVacancySetBySkillLevel(skill, 1));
            }
            for (Vacancy v : vacancySet) {
                List<String> vacancySkillsName = v.getAllSkills().stream().map(x -> x.getName()).collect(Collectors.toList());
                if (v.getActive() && skillsName.containsAll(vacancySkillsName)) {
                    finish.add(v);
                }
            }
            finish.sort((o1, o2) -> o1.getPostName().compareTo(o2.getPostName()));
            Gson gson = new Gson();
            return gson.toJson(finish);
        } catch (ServerException e) {
            return gson.toJson(new ErrorDtoResponse(e.getErrorCode()));
        }
    }

    public String getListOfAllVacanciesMeetsAtLeastOneOfTheRequirementsOfTheEmployerAtNeededLvl(String employeeTokenGson) {
        List<Vacancy> result = new ArrayList<>();
        Map<Integer, ArrayList<Vacancy>> sortedVacancy = new HashMap<>();
        try {
            TokenDtoRequest tokenDto = FromJson.getClassFromJson(employeeTokenGson, TokenDtoRequest.class);
            validateTokenDto(tokenDto);
            Employee employee = employeeDao.getEmployee(tokenDto.getToken());
            List<SkillDtoResponse> skills = employee.getSkillsList();
            List<String> skillsName = skills.stream().map(x -> x.getName()).collect(Collectors.toList());
            Set<Vacancy> vacancySet = new HashSet<>();
            for (String skill : skillsName) {
                vacancySet.addAll(employerDao.getVacancySetBySkillLevel(skill, 1));
            }
            for (Vacancy v : vacancySet) {
                if (v.getActive()) {
                    int workingSkillCount = 0;
                    for (DemandDtoRequest d : v.getDemands()) {
                        int demandSkillLvl = d.getSkill().getLevel();
                        int employeeSkillLvl = employee.getSkillByName(d.getSkill().getName()).getLevel();
                        if (skillsName.contains(d.getSkill().getName()) && demandSkillLvl <= employeeSkillLvl) {
                            workingSkillCount++;
                        }
                    }
                    if (workingSkillCount != 0) {
                        if (sortedVacancy.containsKey(workingSkillCount)) {
                            sortedVacancy.get(workingSkillCount).add(v);
                        } else {
                            ArrayList<Vacancy> newArray = new ArrayList<>();
                            newArray.add(v);
                            sortedVacancy.put(workingSkillCount, newArray);
                        }
                    }
                }
            }
            for (ArrayList<Vacancy> list : sortedVacancy.values()) {
                result.addAll(0, list);
            }
            Gson gson = new Gson();
            return gson.toJson(result);
        } catch (ServerException e) {
            return gson.toJson(new ErrorDtoResponse(e.getErrorCode()));
        }
    }

    private void validateChangeEmployeeDataDto(ChangeEmployeeDataDtoRequest dtoRequest) throws ServerException {
        if (dtoRequest == null) {
            throw new ServerException(ErrorCode.WRONG_INPUT_DATA);
        }
        if (dtoRequest.getName() == null || "".equals(dtoRequest.getName())) {
            throw new ServerException(ErrorCode.WRONG_NAME);
        }
        if (dtoRequest.getPassword() == null || "".equals(dtoRequest.getPassword())) {
            throw new ServerException(ErrorCode.WRONG_PASSWORD);
        }
        if (dtoRequest.getEmail() == null || "".equals(dtoRequest.getEmail())) {
            throw new ServerException(ErrorCode.WRONG_EMAIL);
        }

    }

    private void validateRegisterEmployeeDtoRequest(RegisterEmployeeDtoRequest dtoRequest) throws ServerException {
        if (dtoRequest == null) {
            throw new ServerException(ErrorCode.WRONG_INPUT_DATA);
        }
        if (dtoRequest.getFirstName() == null || "".equals(dtoRequest.getFirstName())) {
            throw new ServerException(ErrorCode.WRONG_NAME);
        }
        if (dtoRequest.getLogin() == null || "".equals(dtoRequest.getLogin())) {
            throw new ServerException(ErrorCode.WRONG_LOGIN);
        }
        if (dtoRequest.getPassword() == null || "".equals(dtoRequest.getPassword())) {
            throw new ServerException(ErrorCode.WRONG_PASSWORD);
        }
        if (dtoRequest.getEmail() == null || "".equals(dtoRequest.getEmail())) {
            throw new ServerException(ErrorCode.WRONG_EMAIL);
        }

    }

    public void clearDataBase() {
        employeeDao.clearEmployeeDataBase();
    }

    // ======================================================== //


}
