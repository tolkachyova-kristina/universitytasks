package net.thumbtack.school.hiring.service;

import com.google.gson.Gson;
import net.thumbtack.school.hiring.dao.EmployerDao;
import net.thumbtack.school.hiring.dao.UserDao;
import net.thumbtack.school.hiring.dto.request.employer.ChangeEmployerDataRequest;
import net.thumbtack.school.hiring.dto.request.employer.RegisterEmployerDtoRequest;
import net.thumbtack.school.hiring.dto.request.user.TokenDtoRequest;
import net.thumbtack.school.hiring.dto.response.baseType.BooleanDtoResponse;
import net.thumbtack.school.hiring.dto.response.baseType.EmptyDtoResponse;
import net.thumbtack.school.hiring.dto.response.modelType.EmployerDataResponse;
import net.thumbtack.school.hiring.dto.response.serviceType.ErrorDtoResponse;
import net.thumbtack.school.hiring.errors.ErrorCode;
import net.thumbtack.school.hiring.errors.ServerException;
import net.thumbtack.school.hiring.model.Employer;
import net.thumbtack.school.hiring.utils.FromJson;
import net.thumbtack.school.hiring.utils.Mode;
import net.thumbtack.school.hiring.utils.Settings;

import java.util.UUID;

public class EmployerService {
    private final EmployerDao employerDao;
    private final UserDao userDao;
    private final Gson gson = new Gson();

    public EmployerService(Mode mode) {
        Settings set = new Settings(mode);
        employerDao = set.getEmployerDao();
        userDao = set.getUserDao();
    }

    private static void validateTokenDto(TokenDtoRequest userTokenDtoJson) throws ServerException {
        if (userTokenDtoJson == null) {
            throw new ServerException(ErrorCode.WRONG_INPUT_DATA);
        }
        if (userTokenDtoJson.getToken() == null || "".compareTo(userTokenDtoJson.getToken()) == 0) {
            throw new ServerException(ErrorCode.WRONG_TOKEN);
        }
    }

    public String registerEmployer(String gsonEmployer) {//проверить оригинальость логина
        try {
            RegisterEmployerDtoRequest registerData = FromJson.getClassFromJson(gsonEmployer, RegisterEmployerDtoRequest.class);
            validateRegisterEmployerDtoRequest(registerData);
            Employer employer = new Employer(registerData);
            userDao.registerUser(employer);
            String token = UUID.randomUUID().toString();
            userDao.logIn(token, employer);
            return gson.toJson(new TokenDtoRequest(token));
        } catch (ServerException e) {
            return gson.toJson(new ErrorDtoResponse(e.getErrorCode()));
        }
    }

    public String changeEmployerData(String newEmployerDataDtoJson) {
        try {
            ChangeEmployerDataRequest employerData = FromJson.getClassFromJson(newEmployerDataDtoJson, ChangeEmployerDataRequest.class);
            validateChangeEmployerData(employerData);
            employerDao.changeEmployerData(employerData);
            return gson.toJson(new EmptyDtoResponse());
        } catch (ServerException e) {
            return gson.toJson(new ErrorDtoResponse(e.getErrorCode()));
        }
    }

    public String getEmployerData(String userTokenDtoJson) {
        try {
            TokenDtoRequest tokenDto = FromJson.getClassFromJson(userTokenDtoJson, TokenDtoRequest.class);
            validateTokenDto(tokenDto);
            Employer em = employerDao.getEmployer(tokenDto.getToken());
            EmployerDataResponse employerData = new EmployerDataResponse(em.getFirstName(), em.getEmail(), em.getCompanyName(), em.getAddress());
            return gson.toJson(employerData);
        } catch (ServerException e) {
            return gson.toJson(new ErrorDtoResponse(e.getErrorCode()));
        }
    }

    public String containsEmployer(String employerGson) {
        try {
            RegisterEmployerDtoRequest registerData = FromJson.getClassFromJson(employerGson, RegisterEmployerDtoRequest.class);
            validateRegisterEmployerDtoRequest(registerData);
            Employer employer = new Employer(registerData);
            Employer em = new Employer(employerDao.getRegisterEmployer(employer.getLogin(), employer.getPassword()));
            boolean t = em.equals(employer);
            return gson.toJson(new BooleanDtoResponse(t));
        } catch (ServerException e) {
            return gson.toJson(new ErrorDtoResponse(e.getErrorCode()));
        }
    }

    private void validateChangeEmployerData(ChangeEmployerDataRequest dtoRequest) throws ServerException {
        if (dtoRequest == null) {
            throw new ServerException(ErrorCode.WRONG_INPUT_DATA);
        }
        if (dtoRequest.getName() == null || "".equals(dtoRequest.getName())) {
            throw new ServerException(ErrorCode.WRONG_NAME);
        }
        if (dtoRequest.getPassword() == null || "".equals(dtoRequest.getPassword())) {
            throw new ServerException(ErrorCode.WRONG_PASSWORD);
        }
        if (dtoRequest.getEmail() == null || "".equals(dtoRequest.getEmail())) {
            throw new ServerException(ErrorCode.WRONG_EMAIL);
        }
        if (dtoRequest.getCompanyName() == null || "".equals(dtoRequest.getCompanyName())) {
            throw new ServerException(ErrorCode.WRONG_COMPANY_NAME);
        }
        if (dtoRequest.getAddress() == null || "".equals(dtoRequest.getAddress())) {
            throw new ServerException(ErrorCode.WRONG_ADDRESS);
        }

    }

    private void validateRegisterEmployerDtoRequest(RegisterEmployerDtoRequest registerDtoRequest) throws ServerException {
        if (registerDtoRequest == null) {
            throw new ServerException(ErrorCode.WRONG_INPUT_DATA);
        }
        if (registerDtoRequest.getFirstName() == null || "".equals(registerDtoRequest.getFirstName())) {
            throw new ServerException(ErrorCode.WRONG_NAME);
        }
        if (registerDtoRequest.getLogin() == null || "".equals(registerDtoRequest.getLogin())) {
            throw new ServerException(ErrorCode.WRONG_LOGIN);
        }
        if (registerDtoRequest.getPassword() == null || "".equals(registerDtoRequest.getPassword())) {
            throw new ServerException(ErrorCode.WRONG_PASSWORD);
        }
        if (registerDtoRequest.getEmail() == null || "".equals(registerDtoRequest.getEmail())) {
            throw new ServerException(ErrorCode.WRONG_EMAIL);
        }
        if (registerDtoRequest.getCompanyName() == null || "".equals(registerDtoRequest.getCompanyName())) {
            throw new ServerException(ErrorCode.WRONG_COMPANY_NAME);
        }
        if (registerDtoRequest.getAddress() == null || "".equals(registerDtoRequest.getAddress())) {
            throw new ServerException(ErrorCode.WRONG_ADDRESS);
        }

    }

    public void clearDataBase() {
        employerDao.clearEmployerDataBase();
    }
}
