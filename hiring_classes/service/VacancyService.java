package net.thumbtack.school.hiring.service;

import com.google.common.collect.Sets;
import com.google.common.collect.TreeMultimap;
import com.google.gson.Gson;
import net.thumbtack.school.hiring.dao.EmployeeDao;
import net.thumbtack.school.hiring.dao.EmployerDao;
import net.thumbtack.school.hiring.dto.request.user.TokenDtoRequest;
import net.thumbtack.school.hiring.dto.request.vacancy.AddVacancyDtoRequest;
import net.thumbtack.school.hiring.dto.request.vacancy.DemandDtoRequest;
import net.thumbtack.school.hiring.dto.request.vacancy.VacancyServiceDtoRequest;
import net.thumbtack.school.hiring.dto.response.baseType.BooleanDtoResponse;
import net.thumbtack.school.hiring.dto.response.baseType.EmptyDtoResponse;
import net.thumbtack.school.hiring.dto.response.modelType.EmployeeDataDtoResponse;
import net.thumbtack.school.hiring.dto.response.modelType.SkillDtoResponse;
import net.thumbtack.school.hiring.dto.response.modelType.VacancyDtoResponse;
import net.thumbtack.school.hiring.dto.response.serviceType.ErrorDtoResponse;
import net.thumbtack.school.hiring.errors.ErrorCode;
import net.thumbtack.school.hiring.errors.ServerException;
import net.thumbtack.school.hiring.model.Employee;
import net.thumbtack.school.hiring.model.Employer;
import net.thumbtack.school.hiring.model.Vacancy;
import net.thumbtack.school.hiring.utils.FromJson;
import net.thumbtack.school.hiring.utils.Mode;
import net.thumbtack.school.hiring.utils.Settings;

import java.util.*;

public class VacancyService {

    private final EmployerDao employerDao;
    private final EmployeeDao employeeDao;
    private final Gson gson = new Gson();

    public VacancyService(Mode mode) {
        Settings set = new Settings(mode);
        employeeDao = set.getEmployeeDao();
        employerDao = set.getEmployerDao();
    }

    private static void validateTokenDto(TokenDtoRequest userTokenDtoJson) throws ServerException {
        if (userTokenDtoJson.getToken() == null || "".compareTo(userTokenDtoJson.getToken()) == 0) {
            throw new ServerException(ErrorCode.WRONG_TOKEN);
        }
    }

    public String addVacancy(String addVacancyDtoRequest) {
        try {
            AddVacancyDtoRequest vacancyData = FromJson.getClassFromJson(addVacancyDtoRequest, AddVacancyDtoRequest.class);
            validateAddVacancyDtoRequest(vacancyData);
            Vacancy vacancy = new Vacancy(vacancyData.getPostName(), vacancyData.getSalary(), vacancyData.getDemands());
            employerDao.addVacancy(vacancyData, vacancy);
            return gson.toJson(new EmptyDtoResponse());
        } catch (ServerException e) {
            return gson.toJson(new ErrorDtoResponse(e.getErrorCode()));
        }
    }

    public VacancyDtoResponse getVacancy(String token, String vacancyName) {
        try {
            return employerDao.getEmployer(token).getVacancy(vacancyName);
        } catch (ServerException e) {
            return null;
        }
    }

    public String getVacancy(String vacancyServiceDto) {
        try {
            VacancyServiceDtoRequest vacancyData = FromJson.getClassFromJson(vacancyServiceDto, VacancyServiceDtoRequest.class);
            validateVacancyServiceDtoRequest(vacancyData);
            Employer em = employerDao.getEmployer(vacancyData.getToken());
            VacancyDtoResponse v = new VacancyDtoResponse(em.getVacancy(vacancyData.getVacancyName()));
            return gson.toJson(v);
        } catch (ServerException e) {
            return gson.toJson(new ErrorDtoResponse(e.getErrorCode()));
        }
    }

    public String madeVacancyActive(String vacancyServiceDto) {
        try {
            VacancyServiceDtoRequest vacancyData = FromJson.getClassFromJson(vacancyServiceDto, VacancyServiceDtoRequest.class);
            validateVacancyServiceDtoRequest(vacancyData);
            employerDao.madeVacancyActive(vacancyData.getToken(), vacancyData.getVacancyName());
            return gson.toJson(new EmptyDtoResponse());
        } catch (ServerException e) {
            return gson.toJson(new ErrorDtoResponse(e.getErrorCode()));
        }
    }

    public String madeVacancyInactive(String vacancyServiceDto) {
        try {
            VacancyServiceDtoRequest vacancyData = FromJson.getClassFromJson(vacancyServiceDto, VacancyServiceDtoRequest.class);
            validateVacancyServiceDtoRequest(vacancyData);
            employerDao.madeVacancyInactive(vacancyData.getToken(), vacancyData.getVacancyName());

            return gson.toJson(new EmptyDtoResponse());
        } catch (ServerException e) {
            return gson.toJson(new ErrorDtoResponse(e.getErrorCode()));
        }
    }

    public String removeVacancy(String vacancyServiceDto) {
        try {
            VacancyServiceDtoRequest vacancyData = FromJson.getClassFromJson(vacancyServiceDto, VacancyServiceDtoRequest.class);
            validateVacancyServiceDtoRequest(vacancyData);
            VacancyDtoResponse vacancy = getVacancy(vacancyData.getToken(), vacancyData.getVacancyName());
            employerDao.removeVacancy(vacancyData, vacancy);


            return gson.toJson(new EmptyDtoResponse());
        } catch (ServerException e) {
            return gson.toJson(new ErrorDtoResponse(e.getErrorCode()));
        }
    }

    public String containsVacancy(String vacancyServiceDto) {
        try {
            VacancyServiceDtoRequest vacancyData = FromJson.getClassFromJson(vacancyServiceDto, VacancyServiceDtoRequest.class);
            validateVacancyServiceDtoRequest(vacancyData);
            Employer em = employerDao.getEmployer(vacancyData.getToken());
            for (VacancyDtoResponse v : em.getVacancyList()) {
                if (v.getPostName().compareTo(vacancyData.getVacancyName()) == 0) {
                    return gson.toJson(new BooleanDtoResponse(true));
                }
            }
            return gson.toJson(new BooleanDtoResponse(false));
        } catch (ServerException e) {
            return gson.toJson(new ErrorDtoResponse(e.getErrorCode()));
        }
    }

    public String vacancyIsActive(String vacancyServiceDto) {
        try {
            VacancyServiceDtoRequest vacancyData = FromJson.getClassFromJson(vacancyServiceDto, VacancyServiceDtoRequest.class);
            validateVacancyServiceDtoRequest(vacancyData);
            return gson.toJson(new BooleanDtoResponse(employerDao.getEmployer(vacancyData.getToken()).getVacancy(vacancyData.getVacancyName()).isActive()));
        } catch (ServerException e) {
            return gson.toJson(new ErrorDtoResponse(e.getErrorCode()));
        }
    }

    public String getListOfActiveVacancy(String userTokenDtoJson) {
        try {
            TokenDtoRequest tokenDto = FromJson.getClassFromJson(userTokenDtoJson, TokenDtoRequest.class);
            validateTokenDto(tokenDto);
            return gson.toJson(employerDao.getListOfActiveVacancy(tokenDto.getToken()));
        } catch (ServerException e) {
            return gson.toJson(new ErrorDtoResponse(e.getErrorCode()));
        }
    }

//==============================================================================//

    public String getListOfInactiveVacancy(String userTokenDtoJson) {
        List<VacancyDtoResponse> newlist = new ArrayList<>();
        try {
            TokenDtoRequest tokenDto = FromJson.getClassFromJson(userTokenDtoJson, TokenDtoRequest.class);
            validateTokenDto(tokenDto);
            return gson.toJson(employerDao.getListOfInactiveVacancy(tokenDto.getToken()));

        } catch (ServerException e) {
            return gson.toJson(new ErrorDtoResponse(e.getErrorCode()));
        }
    }

    public String getListOfEmployeesWhoHaveAllTheNecessarySkillsOnNeededLvl(String vacancyServiceDto) {
        List<EmployeeDataDtoResponse> result = new ArrayList<>();
        Set<Employee> employeesSet = new HashSet<>();
        try {
            //получили список своих скилов
            VacancyServiceDtoRequest vacancyData = FromJson.getClassFromJson(vacancyServiceDto, VacancyServiceDtoRequest.class);
            validateVacancyServiceDtoRequest(vacancyData);
            VacancyDtoResponse vacancy = new VacancyDtoResponse(employerDao.getEmployer(vacancyData.getToken()).getVacancy(vacancyData.getVacancyName()));
            List<SkillDtoResponse> neededSkills = vacancy.getAllSkills();

            employeesSet.addAll(employeeDao.getEmployeeSetBySkillLevel(neededSkills.get(0).getName(), neededSkills.get(0).getLevel()));
            for (SkillDtoResponse skill : neededSkills) {
                employeesSet = Sets.intersection(employeesSet, employeeDao.getEmployeeSetBySkillLevel(skill.getName(), skill.getLevel()));
            }
            for (Employee e : employeesSet) {
                result.add(new EmployeeDataDtoResponse(e.getFirstName(), e.getEmail(), e.getSkillsList()));
            }
            Collections.sort(result, (EmployeeDataDtoResponse o1, EmployeeDataDtoResponse o2) -> o1.getName().compareTo(o2.getName()));

            return gson.toJson(result);
        } catch (ServerException e) {
            return gson.toJson(new ErrorDtoResponse(e.getErrorCode()));
        }

    }

    public String getListOfEmployeesWhoHaveAllTheRequiredSkillsOnNeededLvl(String vacancyServiceDto) {
        List<EmployeeDataDtoResponse> result = new ArrayList<>();
        try {
            VacancyServiceDtoRequest vacancyData = FromJson.getClassFromJson(vacancyServiceDto, VacancyServiceDtoRequest.class);
            validateVacancyServiceDtoRequest(vacancyData);
            VacancyDtoResponse vacancy = new VacancyDtoResponse(employerDao.getEmployer(vacancyData.getToken()).getVacancy(vacancyData.getVacancyName()));
            List<SkillDtoResponse> neededSkills = vacancy.getAllSkills();
            Set<Employee> employeesSet = new HashSet<>();

            employeesSet.addAll(employeeDao.getEmployeeSetBySkillLevel(neededSkills.get(0).getName(), neededSkills.get(0).getLevel()));

            for (DemandDtoRequest d : vacancy.getDemands()) {
                if (d.isMandatory()) {
                    employeesSet = Sets.intersection(employeesSet, employeeDao.getEmployeeSetBySkillLevel(d.getSkill().getName(), d.getSkill().getLevel()));
                } else {
                    employeesSet = Sets.intersection(employeesSet, employeeDao.getEmployeeSetBySkillLevel(d.getSkill().getName(), 1));
                }
            }
            for (Employee e : employeesSet) {
                result.add(new EmployeeDataDtoResponse(e.getFirstName(), e.getEmail(), e.getSkillsList()));
            }
            Collections.sort(result, (EmployeeDataDtoResponse o1, EmployeeDataDtoResponse o2) -> o1.getName().compareTo(o2.getName()));
            return gson.toJson(result);
        } catch (ServerException e) {
            return gson.toJson(new ErrorDtoResponse(e.getErrorCode()));
        }

    }

    public String getListOfEmployeesWhoHaveAllTheNecessarySkillsOnAnyLvl(String vacancyServiceDto) {
        List<EmployeeDataDtoResponse> result = new ArrayList<>();
        try {
            //получили список своих скилов
            VacancyServiceDtoRequest vacancyData = FromJson.getClassFromJson(vacancyServiceDto, VacancyServiceDtoRequest.class);
            validateVacancyServiceDtoRequest(vacancyData);
            VacancyDtoResponse vacancy = new VacancyDtoResponse(employerDao.getEmployer(vacancyData.getToken()).getVacancy(vacancyData.getVacancyName()));
            List<SkillDtoResponse> neededSkills = vacancy.getAllSkills();
            Set<Employee> employeesSet = new HashSet<>();

            employeesSet.addAll(employeeDao.getEmployeeSetBySkillLevel(neededSkills.get(0).getName(), 1));

            for (SkillDtoResponse skill : neededSkills) {
                employeesSet = Sets.intersection(employeesSet, employeeDao.getEmployeeSetBySkillLevel(skill.getName(), 1));
            }
            for (Employee e : employeesSet) {
                result.add(new EmployeeDataDtoResponse(e.getFirstName(), e.getEmail(), e.getSkillsList()));
            }
            Collections.sort(result, (EmployeeDataDtoResponse o1, EmployeeDataDtoResponse o2) -> o1.getName().compareTo(o2.getName()));
            return gson.toJson(result);
        } catch (ServerException e) {
            return gson.toJson(new ErrorDtoResponse(e.getErrorCode()));
        }

    }

    // ======================================================== //

    public String getListOfEmployeesWhoHaveAtLeastOneTheNecessarySkillsOnAnyLvl(String vacancyServiceDto) {
        List<EmployeeDataDtoResponse> result = new ArrayList<>();
        Map<Integer, ArrayList<EmployeeDataDtoResponse>> sortedEmployee = new HashMap<>();
        try {
            //получили список своих скилов
            VacancyServiceDtoRequest vacancyData = FromJson.getClassFromJson(vacancyServiceDto, VacancyServiceDtoRequest.class);
            validateVacancyServiceDtoRequest(vacancyData);
            VacancyDtoResponse vacancy = new VacancyDtoResponse(employerDao.getEmployer(vacancyData.getToken()).getVacancy(vacancyData.getVacancyName()));
            Map<Employee, Integer> employeeMap = new HashMap<>();
            Set<Employee> employeesSet = new HashSet<>();
            for (SkillDtoResponse skill : vacancy.getAllSkills()) {
                employeesSet = employeeDao.getEmployeeSetBySkillLevel(skill.getName(), skill.getLevel());
                for (Employee em : employeesSet) {
                    if (employeeMap.containsKey(em)) {
                        employeeMap.put(em, employeeMap.get(em) + 1);
                    } else {
                        employeeMap.put(em, 1);
                    }
                }
            }
            TreeMultimap<Integer, Employee> tree = TreeMultimap.create((o1, o2) -> o2.compareTo(o1), (o1, o2) -> o1.getFirstName().compareTo(o2.getFirstName()));
            for (Map.Entry<Employee, Integer> entry : employeeMap.entrySet()) {
                tree.put(entry.getValue(), entry.getKey());
            }
            for (Integer key : tree.keySet()) {
                for (Employee e : tree.get(key)) {
                    result.add(new EmployeeDataDtoResponse(e.getFirstName(), e.getEmail(), e.getSkillsList()));
                }

            }
            Collections.sort(result, (EmployeeDataDtoResponse o1, EmployeeDataDtoResponse o2) -> o1.getName().compareTo(o2.getName()));

            return gson.toJson(result);
        } catch (ServerException e) {
            return gson.toJson(new ErrorDtoResponse(e.getErrorCode()));
        }
    }

    private void validateVacancyServiceDtoRequest(VacancyServiceDtoRequest dtoRequest) throws ServerException {
        if (dtoRequest.getToken() == null || "".equals(dtoRequest.getToken())) {
            throw new ServerException(ErrorCode.WRONG_TOKEN);
        }
        if (dtoRequest.getVacancyName() == null || "".equals(dtoRequest.getVacancyName())) {
            throw new ServerException(ErrorCode.WRONG_VACANCY_POST_NAME);
        }
    }

    private void validateAddVacancyDtoRequest(AddVacancyDtoRequest dtoRequest) throws ServerException {
        if (dtoRequest.getToken() == null || "".equals(dtoRequest.getToken())) {
            throw new ServerException(ErrorCode.WRONG_TOKEN);
        }
        if (dtoRequest.getPostName() == null || "".equals(dtoRequest.getPostName())) {
            throw new ServerException(ErrorCode.WRONG_VACANCY_POST_NAME);
        }
        if (dtoRequest.getSalary() <= 0) {
            throw new ServerException(ErrorCode.WRONG_VACANCY_SALARY);
        }
    }

    public void clearDataBase() {
        employerDao.clearVacancyDataBase();
    }
}