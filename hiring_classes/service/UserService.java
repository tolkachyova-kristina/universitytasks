package net.thumbtack.school.hiring.service;

import com.google.gson.Gson;
import net.thumbtack.school.hiring.dao.UserDao;
import net.thumbtack.school.hiring.dto.request.user.LogInDtoRequest;
import net.thumbtack.school.hiring.dto.request.user.TokenDtoRequest;
import net.thumbtack.school.hiring.dto.response.baseType.BooleanDtoResponse;
import net.thumbtack.school.hiring.dto.response.baseType.EmptyDtoResponse;
import net.thumbtack.school.hiring.dto.response.serviceType.ErrorDtoResponse;
import net.thumbtack.school.hiring.errors.ErrorCode;
import net.thumbtack.school.hiring.errors.ServerException;
import net.thumbtack.school.hiring.utils.FromJson;
import net.thumbtack.school.hiring.utils.Mode;
import net.thumbtack.school.hiring.utils.Settings;

import java.util.UUID;

public class UserService {
    private final UserDao userDao;
    private final Gson gson = new Gson();

    public UserService(Mode mode) {
        Settings set = new Settings(mode);
        userDao = set.getUserDao();
    }

    private static void validateLogInDto(LogInDtoRequest logInDtoJson) throws ServerException {
        if (logInDtoJson == null) {
            throw new ServerException(ErrorCode.WRONG_INPUT_DATA);
        }
        if (logInDtoJson.getLogin() == null || logInDtoJson.getLogin().compareTo("") == 0) {
            throw new ServerException(ErrorCode.WRONG_LOGIN);
        }
        if (logInDtoJson.getPassword() == null || logInDtoJson.getPassword().compareTo("") == 0) {
            throw new ServerException(ErrorCode.WRONG_PASSWORD);
        }
    }

    private static void validateTokenDto(TokenDtoRequest userTokenDtoJson) throws ServerException {
        if (userTokenDtoJson == null) {
            throw new ServerException(ErrorCode.WRONG_INPUT_DATA);
        }
        if (userTokenDtoJson.getToken() == null || "".compareTo(userTokenDtoJson.getToken()) == 0) {
            throw new ServerException(ErrorCode.WRONG_TOKEN);
        }
    }

    public String logIn(String logInDtoGson) {
        try {
            LogInDtoRequest logInDto = FromJson.getClassFromJson(logInDtoGson, LogInDtoRequest.class);
            validateLogInDto(logInDto);
            String token = UUID.randomUUID().toString();
            userDao.logIn(token, userDao.getUser(logInDto.getLogin(), logInDto.getPassword()));
            return gson.toJson(new TokenDtoRequest(token));
        } catch (ServerException e) {
            return gson.toJson(new ErrorDtoResponse(e.getErrorCode()));
        }
    }

    public String logOut(String userTokenDtoJson) {
        try {
            TokenDtoRequest tokenDto = FromJson.getClassFromJson(userTokenDtoJson, TokenDtoRequest.class);
            validateTokenDto(tokenDto);
            userDao.logOut(tokenDto.getToken());
            return gson.toJson(new EmptyDtoResponse());
        } catch (ServerException e) {
            return gson.toJson(new ErrorDtoResponse(e.getErrorCode()));
        }
    }

    // ======================================================== //

    public String removeUser(String userTokenDtoJson) {
        try {
            TokenDtoRequest tokenDto = FromJson.getClassFromJson(userTokenDtoJson, TokenDtoRequest.class);
            validateTokenDto(tokenDto);
            userDao.removeUser(tokenDto.getToken());
            return gson.toJson(new EmptyDtoResponse());
        } catch (ServerException e) {
            return gson.toJson(new ErrorDtoResponse(e.getErrorCode()));
        }
    }

    public String isLogIn(String userTokenDtoJson) {
        try {
            TokenDtoRequest tokenDto = FromJson.getClassFromJson(userTokenDtoJson, TokenDtoRequest.class);
            validateTokenDto(tokenDto);
            return gson.toJson(new BooleanDtoResponse(userDao.isLogIn(tokenDto.getToken())));
        } catch (ServerException e) {
            return gson.toJson(new ErrorDtoResponse(e.getErrorCode()));
        }
    }
}
