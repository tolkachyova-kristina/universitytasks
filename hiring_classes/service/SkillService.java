package net.thumbtack.school.hiring.service;

import com.google.gson.Gson;
import net.thumbtack.school.hiring.dao.EmployeeDao;
import net.thumbtack.school.hiring.dto.request.skill.AddNewSkillDtoRequest;
import net.thumbtack.school.hiring.dto.request.skill.ChangeSkillLevelDtoRequest;
import net.thumbtack.school.hiring.dto.request.skill.SkillServiceDtoRequest;
import net.thumbtack.school.hiring.dto.request.user.TokenDtoRequest;
import net.thumbtack.school.hiring.dto.response.baseType.BooleanDtoResponse;
import net.thumbtack.school.hiring.dto.response.baseType.EmptyDtoResponse;
import net.thumbtack.school.hiring.dto.response.baseType.IntegerDtoResponse;
import net.thumbtack.school.hiring.dto.response.serviceType.ErrorDtoResponse;
import net.thumbtack.school.hiring.errors.ErrorCode;
import net.thumbtack.school.hiring.errors.ServerException;
import net.thumbtack.school.hiring.model.Skill;
import net.thumbtack.school.hiring.utils.FromJson;
import net.thumbtack.school.hiring.utils.Mode;
import net.thumbtack.school.hiring.utils.Settings;

public class SkillService {
    private final EmployeeDao employeeDao;
    private final Gson gson = new Gson();

    public SkillService(Mode mode) {
        Settings set = new Settings(mode);
        employeeDao = set.getEmployeeDao();
    }

    private static void validateTokenDto(TokenDtoRequest userTokenDtoJson) throws ServerException {
        if (userTokenDtoJson.getToken() == null || "".compareTo(userTokenDtoJson.getToken()) == 0) {
            throw new ServerException(ErrorCode.WRONG_TOKEN);
        }
    }

    public String addNewSkill(String addSkillDtoRequest) {
        try {
            AddNewSkillDtoRequest skillData = FromJson.getClassFromJson(addSkillDtoRequest, AddNewSkillDtoRequest.class);
            validateAddNewSkillDtoRequest(skillData);
            Skill skill = new Skill(skillData.getSkillName(), skillData.getSkillLevel());
            employeeDao.addNewSkill(skill, skillData.getToken());
            return gson.toJson(new EmptyDtoResponse());
        } catch (ServerException e) {
            return gson.toJson(new ErrorDtoResponse(e.getErrorCode()));
        }
    }

    public String removeSkill(String skillServiceDto) {
        try {
            SkillServiceDtoRequest skillData = FromJson.getClassFromJson(skillServiceDto, SkillServiceDtoRequest.class);
            validateSkillServiceDtoRequest(skillData);
            employeeDao.removeSkill(skillData.getToken(), skillData.getSkillName());

            return gson.toJson(new EmptyDtoResponse());
        } catch (ServerException e) {
            return gson.toJson(new ErrorDtoResponse(e.getErrorCode()));
        }
    }

    public String changeSkillLevel(String changeSkillLevelDto) {
        try {
            ChangeSkillLevelDtoRequest skillData = FromJson.getClassFromJson(changeSkillLevelDto, ChangeSkillLevelDtoRequest.class);
            validateChangeSkillLevelDtoRequest(skillData);
            employeeDao.changeSkillLevel(skillData);
            return gson.toJson(new EmptyDtoResponse());
        } catch (ServerException e) {
            return gson.toJson(new ErrorDtoResponse(e.getErrorCode()));
        }
    }

    public String containsSkill(String skillServiceDto) {
        try {
            SkillServiceDtoRequest skillData = FromJson.getClassFromJson(skillServiceDto, SkillServiceDtoRequest.class);
            validateSkillServiceDtoRequest(skillData);
            return gson.toJson(new BooleanDtoResponse(employeeDao.containsSkill(skillData.getSkillName(), skillData.getToken())));
        } catch (ServerException e) {
            return gson.toJson(new ErrorDtoResponse(e.getErrorCode()));
        }
    }

    // ======================================================== //

    public String getSkillLevel(String skillServiceDto) {
        try {
            SkillServiceDtoRequest skillData = FromJson.getClassFromJson(skillServiceDto, SkillServiceDtoRequest.class);
            validateSkillServiceDtoRequest(skillData);
            return gson.toJson(new IntegerDtoResponse(employeeDao.getSkillLevel(skillData.getToken(), skillData.getSkillName())));
        } catch (ServerException e) {
            return gson.toJson(new ErrorDtoResponse(e.getErrorCode()));
        }
    }

    private void validateAddNewSkillDtoRequest(AddNewSkillDtoRequest dtoRequest) throws ServerException {
        if (dtoRequest == null) {
            throw new ServerException(ErrorCode.WRONG_INPUT_DATA);
        }
        if (dtoRequest.getToken() == null || "".compareTo(dtoRequest.getToken()) == 0) {
            throw new ServerException(ErrorCode.WRONG_SKILL_NAME);
        }
        if (dtoRequest.getSkillName() == null || "".compareTo(dtoRequest.getSkillName()) == 0) {
            throw new ServerException(ErrorCode.WRONG_SKILL_NAME);
        }
        if (dtoRequest.getSkillLevel() < 1 || dtoRequest.getSkillLevel() > 5) {
            throw new ServerException(ErrorCode.WRONG_SKILL_LEVEL);
        }
    }

    private void validateChangeSkillLevelDtoRequest(ChangeSkillLevelDtoRequest dtoRequest) throws ServerException {
        if (dtoRequest.getToken() == null || "".equals(dtoRequest.getToken())) {
            throw new ServerException(ErrorCode.WRONG_TOKEN);
        }
        if (dtoRequest.getSkillName() == null || "".equals(dtoRequest.getSkillName())) {
            throw new ServerException(ErrorCode.WRONG_SKILL_NAME);
        }
        if (dtoRequest.getNewLevel() < 1 || dtoRequest.getNewLevel() > 5) {
            throw new ServerException(ErrorCode.WRONG_SKILL_LEVEL);
        }
    }

    private void validateSkillServiceDtoRequest(SkillServiceDtoRequest skillServiceDto) throws ServerException {
        if (skillServiceDto.getToken() == null || "".equals(skillServiceDto.getToken())) {
            throw new ServerException(ErrorCode.WRONG_TOKEN);
        }
        if (skillServiceDto.getSkillName() == null || "".equals(skillServiceDto.getSkillName())) {
            throw new ServerException(ErrorCode.WRONG_VACANCY_POST_NAME);
        }
    }

    public void clearDataBase() {
        employeeDao.clearSkillDataBase();
    }
}
