package net.thumbtack.school.hiring.dao;

import net.thumbtack.school.hiring.dto.request.employer.ChangeEmployerDataRequest;
import net.thumbtack.school.hiring.dto.request.vacancy.AddVacancyDtoRequest;
import net.thumbtack.school.hiring.dto.request.vacancy.VacancyServiceDtoRequest;
import net.thumbtack.school.hiring.dto.response.modelType.VacancyDtoResponse;
import net.thumbtack.school.hiring.errors.ServerException;
import net.thumbtack.school.hiring.model.Employer;
import net.thumbtack.school.hiring.model.Vacancy;

import java.util.List;
import java.util.Set;

public interface EmployerDao {
    Employer getEmployer(String token) throws ServerException;

    List<Employer> getAllEmployers();

    Employer getRegisterEmployer(String login, String password) throws ServerException;

    Set<Vacancy> getVacancySetBySkillLevel(String skillName, int minLevel);

    void removeVacancyFromTree(VacancyDtoResponse vacancyDto) throws ServerException;

    void clearVacancyDataBase();

    void clearEmployerDataBase();

    void changeEmployerData(ChangeEmployerDataRequest employerData) throws ServerException;


    void addVacancy(AddVacancyDtoRequest vacancyData, Vacancy vacancy) throws ServerException;

    void madeVacancyActive(String token, String vacancyName) throws ServerException;

    void madeVacancyInactive(String token, String vacancyName) throws ServerException;

    void removeVacancy(VacancyServiceDtoRequest vacancyData, VacancyDtoResponse vacancy) throws ServerException;

    List<VacancyDtoResponse> getListOfActiveVacancy(String token) throws ServerException;

    List<VacancyDtoResponse> getListOfInactiveVacancy(String token) throws ServerException;

}
