package net.thumbtack.school.hiring.dao;

import net.thumbtack.school.hiring.errors.ServerException;
import net.thumbtack.school.hiring.model.User;

public interface UserDao {
    void logIn(String token, User user) throws ServerException;

    void logOut(String token) throws ServerException;

    User getUser(String login, String password) throws ServerException;

    void registerUser(User user) throws ServerException;

    void removeUser(String token) throws ServerException;

    boolean isLogIn(String userToken);
}
