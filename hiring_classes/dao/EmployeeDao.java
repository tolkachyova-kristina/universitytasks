package net.thumbtack.school.hiring.dao;


import net.thumbtack.school.hiring.dto.request.employee.ChangeEmployeeDataDtoRequest;
import net.thumbtack.school.hiring.dto.request.skill.ChangeSkillLevelDtoRequest;
import net.thumbtack.school.hiring.errors.ServerException;
import net.thumbtack.school.hiring.model.Employee;
import net.thumbtack.school.hiring.model.Skill;

import java.util.List;
import java.util.Set;

public interface EmployeeDao {
    Employee getEmployee(String token) throws ServerException;

    List<Employee> getAllEmployees();

    Employee getRegisterEmployee(String login, String password) throws ServerException;

    void addEmployeeInSortedTree(Employee employee, Skill skill);

    Set<Employee> getEmployeeSetBySkillLevel(String skillNamel, int minLevel);

    void clearSkillDataBase();

    void clearEmployeeDataBase();

    void changeEmployeeData(ChangeEmployeeDataDtoRequest employeeData) throws ServerException;

    boolean employeeIsActive(String token) throws ServerException;

    void madeAccountInactive(String userTokenDtoJson) throws ServerException;

    void madeAccountActive(String token) throws ServerException;

    void addNewSkill(Skill skill, String token) throws ServerException;

    boolean containsSkill(String skillName, String token) throws ServerException;

    void removeSkill(String token, String skillName) throws ServerException;

    void changeSkillLevel(ChangeSkillLevelDtoRequest skillData) throws ServerException;

    int getSkillLevel(String token, String skillName) throws ServerException;
}
