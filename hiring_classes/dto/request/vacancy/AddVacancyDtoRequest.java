package net.thumbtack.school.hiring.dto.request.vacancy;

import net.thumbtack.school.hiring.dto.response.modelType.SkillDtoResponse;
import net.thumbtack.school.hiring.errors.ErrorCode;
import net.thumbtack.school.hiring.errors.ServerException;

import java.util.ArrayList;
import java.util.List;

public class AddVacancyDtoRequest {
    private final String token;
    private final String postName;
    private final Integer salary;
    private final List<DemandDtoRequest> demands;

    public AddVacancyDtoRequest(String token, String postName, Integer salary) {
        this.token = token;
        this.postName = postName;
        this.salary = salary;
        this.demands = new ArrayList<>();
    }

    public String getToken() {
        return token;
    }

    public String getPostName() {
        return postName;
    }

    public Integer getSalary() {
        return salary;
    }

    public List<DemandDtoRequest> getDemands() {
        return demands;
    }

    public void addDemand(String skillName, int skillLevel, boolean mandatory) throws ServerException {
        if (skillName == null || "".compareTo(skillName) == 0) {
            throw new ServerException(ErrorCode.WRONG_SKILL_NAME);
        }
        if (skillLevel < 1 || skillLevel > 5) {
            throw new ServerException(ErrorCode.WRONG_SKILL_LEVEL);
        }
        demands.add(new DemandDtoRequest(new SkillDtoResponse(skillName, skillLevel), mandatory));
    }

}
