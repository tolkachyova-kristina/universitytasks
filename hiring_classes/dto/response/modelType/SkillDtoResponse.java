package net.thumbtack.school.hiring.dto.response.modelType;

import net.thumbtack.school.hiring.errors.ErrorCode;
import net.thumbtack.school.hiring.errors.ServerException;

import java.util.Objects;

public class SkillDtoResponse {
    private final int id;
    private String name;
    private int level;

    public SkillDtoResponse(String name, int lvl) {
        this.name = name;
        this.level = lvl;
        id = -1;
    }

    public SkillDtoResponse(int id, String name, int lvl) {
        this.name = name;
        this.level = lvl;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void validate() throws ServerException {
        if (name == null || "".compareTo(name) == 0) {
            throw new ServerException(ErrorCode.WRONG_SKILL_NAME);
        }
        if (level < 1 || level > 5) {
            throw new ServerException(ErrorCode.WRONG_SKILL_LEVEL);
        }

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SkillDtoResponse that = (SkillDtoResponse) o;
        return level == that.level &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, level);
    }

    @Override
    public String toString() {
        return "{" +
                "\"id\":" + id +
                ", \"name\":\"" + name +
                "\", \"level\":" + level +
                "}";
    }
}
