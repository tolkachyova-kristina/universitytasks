package net.thumbtack.school.hiring.dto.response.baseType;

public class IntegerDtoResponse {
    private final int value;

    public IntegerDtoResponse(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
