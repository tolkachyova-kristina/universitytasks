package net.thumbtack.school.hiring.errors;

public class ServerException extends Exception {
    private final ErrorCode error;

    public ServerException(ErrorCode error) {
        this.error = error;
    }

    public String getErrorCode() {
        return error.toString();
    }
}
