package net.thumbtack.school.hiring;

import net.thumbtack.school.hiring.database.DataBase;
import net.thumbtack.school.hiring.mybatis.utils.SessionFactory;
import net.thumbtack.school.hiring.service.*;
import net.thumbtack.school.hiring.utils.Mode;

public class Server {
    static EmployeeService EMPLOYEE_SERVICE;
    static EmployerService EMPLOYER_SERVICE;
    static UserService USER_SERVICE;
    static VacancyService VACANCY_SERVICE;
    static SkillService SKILL_SERVICE;


    public static void startServer(String savedDataFileName, Mode mode) {
        DataBase.getInstance(savedDataFileName);
        if (mode != null) {
            SessionFactory.initSqlSessionFactory();
        }
        EMPLOYEE_SERVICE = new EmployeeService(mode);
        EMPLOYER_SERVICE = new EmployerService(mode);
        USER_SERVICE = new UserService(mode);
        VACANCY_SERVICE = new VacancyService(mode);
        SKILL_SERVICE = new SkillService(mode);
    }

    public static void stopServer() {
        EMPLOYEE_SERVICE.clearDataBase();
        EMPLOYER_SERVICE.clearDataBase();
        VACANCY_SERVICE.clearDataBase();
        SKILL_SERVICE.clearDataBase();
        DataBase.getDown();
    }

    public static String logIn(String logInDtoJson) {
        return USER_SERVICE.logIn(logInDtoJson);
    }
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//

    public static String logOut(String userTokenDtoJson) {
        return USER_SERVICE.logOut(userTokenDtoJson);
    }

    public static String isLogIn(String userTokenDtoJson) {
        return USER_SERVICE.isLogIn(userTokenDtoJson);
    }

    public static String containsEmployer(String employerJson) {
        return EMPLOYER_SERVICE.containsEmployer(employerJson);
    }

    public static String containsEmployee(String employeeJson) {
        return EMPLOYEE_SERVICE.containsEmployee(employeeJson);
    }

    public static String removeUser(String userTokenDtoJson) {
        return USER_SERVICE.removeUser(userTokenDtoJson);
    }

    public static String registerEmployer(String gsonEmployer) {
        return EMPLOYER_SERVICE.registerEmployer(gsonEmployer); //+
    }

    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//

    public static String changeEmployerData(String newEmployerDataDtoJson) {
        return EMPLOYER_SERVICE.changeEmployerData(newEmployerDataDtoJson);//+
    }

    public static String getEmployerData(String userTokenDtoJson) {
        return EMPLOYER_SERVICE.getEmployerData(userTokenDtoJson);
    }

    public static String registerEmployee(String gsonEmployee) {
        return EMPLOYEE_SERVICE.registerEmployee(gsonEmployee);//+
    }
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//

    public static String changeEmployeeData(String newEmployeeDataDtoJson) {
        return EMPLOYEE_SERVICE.changeEmployeeData(newEmployeeDataDtoJson);//+
    }

    public static String getEmployeeData(String userTokenDtoJson) {
        return EMPLOYEE_SERVICE.getEmployeeData(userTokenDtoJson);
    }

    public static String madeAccountInactive(String userTokenDtoJson) {
        return EMPLOYEE_SERVICE.madeAccountInactive(userTokenDtoJson);//+
    }

    public static String madeAccountActive(String userTokenDtoJson) {
        return EMPLOYEE_SERVICE.madeAccountActive(userTokenDtoJson);//+
    }

    public static String employeeIsActive(String userTokenDtoJson) {
        return EMPLOYEE_SERVICE.employeeIsActive(userTokenDtoJson);
    }

    public static String addVacancy(String addVacancyDtoRequest) {
        return VACANCY_SERVICE.addVacancy(addVacancyDtoRequest);//+
    }
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//

    public static String getVacancy(String vacancyServiceDto) {
        return VACANCY_SERVICE.getVacancy(vacancyServiceDto);//+
    }

    public static String madeVacancyActive(String vacancyServiceDto) {
        return VACANCY_SERVICE.madeVacancyActive(vacancyServiceDto);//+
    }

    public static String madeVacancyInactive(String vacancyServiceDto) {
        return VACANCY_SERVICE.madeVacancyInactive(vacancyServiceDto);//+
    }

    public static String removeVacancy(String vacancyServiceDto) {
        return VACANCY_SERVICE.removeVacancy(vacancyServiceDto);//+
    }

    public static String getListOfActiveVacancy(String userTokenDtoJson) {
        return VACANCY_SERVICE.getListOfActiveVacancy(userTokenDtoJson);//+
    }

    public static String getListOfInactiveVacancy(String userTokenDtoJson) {
        return VACANCY_SERVICE.getListOfInactiveVacancy(userTokenDtoJson);//+
    }

    public static String containsVacancy(String vacancyServiceDto) {
        return VACANCY_SERVICE.containsVacancy(vacancyServiceDto);
    }

    public static String vacancyIsActive(String vacancyServiceDto) {
        return VACANCY_SERVICE.vacancyIsActive(vacancyServiceDto);
    }

    public static String addNewSkill(String addSkillDtoRequest) {
        return SKILL_SERVICE.addNewSkill(addSkillDtoRequest);//+
    }
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//

    public static String removeSkill(String skillServiceDto) {
        return SKILL_SERVICE.removeSkill(skillServiceDto);//+
    }

    public static String changeSkillLevel(String changeSkillLevelDto) {
        return SKILL_SERVICE.changeSkillLevel(changeSkillLevelDto);//+
    }

    public static String getSkillLevel(String skillServiceDto) {
        return SKILL_SERVICE.getSkillLevel(skillServiceDto);//+
    }

    public static String containsSkill(String skillServiceDto) {
        return SKILL_SERVICE.containsSkill(skillServiceDto);
    }

    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
    //список вакансий, все умения на необходимом уровне
    public static String getListOfAllVacanciesMeetsTheRequirementsOfTheEmployerAtANeededLvl(String employeeTokenDto) {
        return EMPLOYEE_SERVICE.getListOfAllVacanciesMeetsTheRequirementsOfTheEmployerAtANeededLvl(employeeTokenDto);
    }

    //список вакансий, только обязательные умения на необходимом уровне
    public static String getListOfAllVacanciesMeetsTheBindingRequirementsOfTheEmployerAtANeededLvl(String employeeTokenDto) {
        return EMPLOYEE_SERVICE.getListOfAllVacanciesMeetsTheBindingRequirementsOfTheEmployerAtANeededLvl(employeeTokenDto);
    }

    //список вакансий, все умения на лбом уровне
    public static String getListOfAllVacanciesMeetsTheRequirementsOfTheEmployerOnAnyLvl(String employeeTokenDto) {
        return EMPLOYEE_SERVICE.getListOfAllVacanciesMeetsTheRequirementsOfTheEmployerOnAnyLvl(employeeTokenDto);
    }

    //список вакансий, есть хоть одно умения на необходимом уровне
    public static String getListOfAllVacanciesMeetsAtLeastOneOfTheRequirementsOfTheEmployerAtNeededLvl(String employeeTokenDto) {
        return EMPLOYEE_SERVICE.getListOfAllVacanciesMeetsAtLeastOneOfTheRequirementsOfTheEmployerAtNeededLvl(employeeTokenDto);
    }

    public static String getListOfEmployeesWhoHaveAllTheNecessarySkillsOnNeededLvl(String vacancyServiceDto) {
        return VACANCY_SERVICE.getListOfEmployeesWhoHaveAllTheNecessarySkillsOnNeededLvl(vacancyServiceDto);
    }

    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//

    public static String getListOfEmployeesWhoHaveAllTheRequiredSkillsOnNeededLvl(String vacancyServiceDto) {
        return VACANCY_SERVICE.getListOfEmployeesWhoHaveAllTheRequiredSkillsOnNeededLvl(vacancyServiceDto);
    }

    public static String getListOfEmployeesWhoHaveAllTheNecessarySkillsOnAnyLvl(String vacancyServiceDto) {
        return VACANCY_SERVICE.getListOfEmployeesWhoHaveAllTheNecessarySkillsOnAnyLvl(vacancyServiceDto);
    }

    public static String getListOfEmployeesWhoHaveAtLeastOneTheNecessarySkillsOnAnyLvl(String vacancyServiceDto) {
        return VACANCY_SERVICE.getListOfEmployeesWhoHaveAtLeastOneTheNecessarySkillsOnAnyLvl(vacancyServiceDto);
    }

    public void stopServer(String saveDataFileName) {
    }

}

// Что должен делать сервер:
//+зарегистрировать нового работника/работодателя
//+работник/работодатель может изменить свои лич.данные кроме логина
//+зарегистрированный работник/работодатель может заходить и покидать сервер ч/з логин и пароль
//+зарегистрированный работник/работодатель может покинуть сервер и его данные удаляться
//+зарег. работодатель может добавить свои вакансии по образцу
//+зарег. работодатель может изменить/удалить/сделать неактивной свою вакансию
//+зарег. работодатель может просмотреть весь/активный/неактивный список своих вакансий
//+зарег. работник может добавить/удалить в профиль умение, изменить его уровень
//+зарег. работник может сделать свой профиль неактивным
//>>общий список требований/умений из которых работник/работодатель может выбирать, пополняется пользователями
//
// В любой момент работодатель может получить	для каждой своей вакансии
//-список всех потенциальных работников, имеющих все необходимые для этой вакансии умения на уровне не ниже уровня, указанного в требовании
//-список всех потенциальных работников, имеющих все обязательные требования на уровне не ниже уровня, указанного в требовании
//-список всех потенциальных работников, имеющих все необходимые для этой вакансии умения на любом уровне
//-список всех потенциальных работников, имеющих хотя бы одно  необходимое для этой вакансии умение на уровне не ниже уровня, указанного в требовании
//
//В любой момент потенциальный работник может получить
// + список всех вакансий работодателей, для которых его набор умений соответствует требованиям работодателя на уровне не ниже
// уровня, указанного в требовании
// +список всех вакансий работодателей, для которых его набор умений соответствует обязательным требованиям работодателя на
//  уровне не ниже уровня, указанного в требовании
// +список всех вакансий работодателей, для которых его набор умений соответствует требованиям работодателя на любом уровне
// +-список всех вакансий работодателей, для которых работник имеет хотя бы одно умение из списка в требовании работодателя на
//  уровне не ниже уровня, указанного в требовании. В этом случае список выдается отсортированным по числу найденных умений,
//  то есть в начале списка приводятся те вакансии работодателей, для которых работник имеет большее число умений.

//-работодатель может принять кандидата на нужную вакансию, тогда работник и вакансия становятся неактивными.

//название должности
//предполагаемый оклад
//список требований к работникам
//	Каждое требование к работнику состоит из 3 элементов
//название требования - текстовая строка. Например, “язык Java”
//уровень владения этим требованием потенциального работника. Определяется по шкале “1” - “5” (1- начальный уровень, 5 - максимально высокий)
//является ли требование обязательным




