package net.thumbtack.school.hiring.mybatis.mappers;

import net.thumbtack.school.hiring.dto.request.employer.ChangeEmployerDataRequest;
import net.thumbtack.school.hiring.model.Employer;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

import java.util.List;

public interface EmployerMapper {

    @Select("SELECT firstname, email,login, password,id, companyName, address FROM employer WHERE token = #{token}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "vacancyList", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.hiring.mybatis.mappers.VacancyMapper.getVacancyDtoListByEmployerID", fetchType = FetchType.LAZY)),
    })
    Employer getEmployer(@Param("token") String token);

    @Select("SELECT firstname, email,login, password, companyName, address FROM employer")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "vacancyList", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.hiring.mybatis.mappers.VacancyMapper.getListByEmployerID", fetchType = FetchType.LAZY)),
    })
    List<Employer> getAllEmployers();

    @Select("SELECT firstname, email,login, password, id, companyName, address FROM employer WHERE login = #{login} and password = #{password}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "vacancyList", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.hiring.mybatis.mappers.VacancyMapper.getListByEmployerID", fetchType = FetchType.LAZY)),
    })
    Employer getRegisterEmployer(@Param("login") String login, @Param("password") String password);

    @Update("UPDATE employer SET token = #{token} WHERE login = #{user.login} and password = #{user.password} ")
    void logIn(@Param("token") String token, @Param("user") Employer user);

    @Insert("INSERT INTO employer ( firstname, email, login, password, companyName, address) VALUES "
            + "( #{employer.firstName}, #{employer.email}, #{employer.login}, " +
            "#{employer.password} ,#{employer.companyName} ,#{employer.address})")
    @Options(useGeneratedKeys = true)
    void registerEmployer(@Param("employer") Employer employer);

    @Update("UPDATE employer SET token = null WHERE token = #{token}")
    void logOut(String token);

    @Delete("DELETE FROM employer WHERE token = #{token}")
    void remove(String token);

    @Select("SELECT id FROM employer WHERE token = #{userToken}")
    Integer getLogInEmployer(String userToken);

    @Delete("DELETE FROM employer")
    void clear();

    @Select("SELECT login FROM employer")
    List<String> getLoginList();

    @Update("UPDATE employer SET firstname = #{data.name}, email= #{data.email}, password= #{data.password}, " +
            "companyName = #{data.companyName}, address = #{data.address}  WHERE token = #{data.token}")
    void changeEmployerData(@Param("data") ChangeEmployerDataRequest employerData);
}
