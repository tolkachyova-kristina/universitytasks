package net.thumbtack.school.hiring.mybatis.mappers;

import net.thumbtack.school.hiring.dto.response.modelType.SkillDtoResponse;
import net.thumbtack.school.hiring.model.Skill;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface SkillMapper {
    @Select("SELECT * FROM skill WHERE id IN (SELECT skillID FROM employee_skill WHERE employeeID = #{id})")
    List<Skill> getListByEmployeeID(int id);

    @Select("SELECT * FROM skill WHERE id IN (SELECT skillID FROM employee_skill WHERE employeeID = #{id})")
    List<SkillDtoResponse> getSkillDtoListByEmployeeID(int id);

    @Insert("INSERT INTO skill ( name, `level`) VALUES " + "(#{skill.name} ,#{skill.level})")
    @Options(useGeneratedKeys = true, keyProperty = "skill.id")
    void insertSkill(@Param("skill") Skill skill);

    @Delete("DELETE FROM skill WHERE id = #{skillID}")
    void removeSkill(@Param("skillID") int skillID);

    @Select("SELECT skillID FROM employee_skill WHERE employeeID = #{employeeID} AND skillID IN (SELECT id FROM skill WHERE name = #{skillName})")
    int findSkillIDByEmployee(@Param("employeeID") int employeeID, @Param("skillName") String skillName);

    @Insert("INSERT INTO employee_skill ( employeeID, skillID) VALUES " + "(#{employeeID} ,#{skill.id})")
    @Options(useGeneratedKeys = true)
    void addSkillToEmployee(@Param("employeeID") int employeeID, @Param("skill") Skill skill);

    @Delete("DELETE FROM skill")
    void clear();

    @Update("UPDATE skill SET `level` = #{newLevel} WHERE id = #{skillID} ")
    void changeSkillLevel(@Param("skillID") int skillID, @Param("newLevel") int newLevel);

    @Select("SELECT `level` FROM skill WHERE id = #{skillID}")
    int getSkillLevel(@Param("skillID") int skillID);
}
