package net.thumbtack.school.hiring.mybatis.mappers;

import net.thumbtack.school.hiring.dto.request.employee.ChangeEmployeeDataDtoRequest;
import net.thumbtack.school.hiring.model.Employee;
import net.thumbtack.school.hiring.model.Skill;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

import java.util.List;
import java.util.Set;

public interface EmployeeMapper {
    @Insert("INSERT INTO employee ( firstname, email, login, password, isActive) VALUES "
            + "( #{employee.firstName}, #{employee.email}, #{employee.login}, #{employee.password} ,#{employee.isActive} )")
    @Options(useGeneratedKeys = true)
    void insertEmployee(@Param("employee") Employee employee);

    @Select("SELECT firstname, email,login, password,id, isActive FROM employee WHERE token = #{token}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "skillsList", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.hiring.mybatis.mappers.SkillMapper.getSkillDtoListByEmployeeID", fetchType = FetchType.LAZY)),
    })
    Employee getEmployee(@Param("token") String token);

    @Select("SELECT firstname, email,login, password, isActive FROM employee")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "skillsList", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.hiring.mybatis.mappers.SkillMapper.getListByEmployeeID", fetchType = FetchType.LAZY)),
    })
    List<Employee> getAllEmployees();

    @Select("SELECT firstname, email,login, password,id, isActive FROM employee WHERE login = #{login} and password = #{password}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "skillsList", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.hiring.mybatis.mappers.SkillMapper.getListByEmployeeID", fetchType = FetchType.LAZY)),
    })
    Employee getRegisterEmployee(@Param("login") String login, @Param("password") String password);


    @Select("SELECT * FROM employee WHERE id IN (SELECT employeeID FROM employee_skill WHERE skillID = #{skill.id})")
    Set<Employee> getEmployeeSetBySkill(@Param("skill") Skill skill);

    @Select("SELECT firstname, email,login, password,id, isActive FROM employee WHERE id IN (SELECT employeeID FROM employee_skill WHERE skillID IN (SELECT id FROM skill WHERE name = #{skillName} and `level` >= #{minLevel}))")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "skillsList", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.hiring.mybatis.mappers.SkillMapper.getListByEmployeeID", fetchType = FetchType.LAZY)),
    })
    Set<Employee> getEmployeeSetBySkillLevel(@Param("skillName") String skillName, @Param("minLevel") int minLevel);

    @Update("UPDATE employee SET token = #{token} WHERE login = #{user.login} and password = #{user.password} ")
    void logIn(@Param("token") String token, @Param("user") Employee user);

    @Update("UPDATE employee SET token = null WHERE token = #{token}")
    void logOut(@Param("token") String token);

    @Delete("DELETE FROM employee WHERE token = #{token}")
    void remove(@Param("token") String token);


    @Select("SELECT id FROM employee WHERE token = #{userToken}")
    Integer getLogInEmployee(String userToken);

    @Delete("DELETE FROM employee")
    void clear();

    @Select("SELECT login FROM employee")
    List<String> getLoginList();

    @Update("UPDATE employee SET firstname = #{employeeData.name}, email= #{employeeData.email}, password= #{employeeData.password} WHERE token = #{employeeData.token}")
    void changeEmployeeData(@Param("employeeData") ChangeEmployeeDataDtoRequest employeeData);

    @Select("SELECT isActive FROM employee where token = #{token}")
    int employeeIsActive(@Param("token") String token);

    @Update("UPDATE employee SET isActive = 0 WHERE token = #{token}")
    void madeAccountInactive(@Param("token") String userTokenDtoJson);

    @Update("UPDATE employee SET isActive = 1 WHERE token = #{token}")
    void madeAccountActive(@Param("token") String token);

    @Select("SELECT id FROM employee where token = #{token}")
    int getId(@Param("token") String token);
}
