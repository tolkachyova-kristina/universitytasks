package net.thumbtack.school.hiring.mybatis.mappers;

import net.thumbtack.school.hiring.dto.request.vacancy.DemandDtoRequest;
import net.thumbtack.school.hiring.dto.response.modelType.VacancyDtoResponse;
import net.thumbtack.school.hiring.model.Skill;
import net.thumbtack.school.hiring.model.Vacancy;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

import java.util.List;
import java.util.Set;

public interface VacancyMapper {

    @Insert("INSERT INTO vacancy ( postName, salary, isActive, employerID) VALUES " + "( #{vacancy.postName}, #{vacancy.salary}, #{vacancy.isActive},#{employerID} )")
    @Options(useGeneratedKeys = true, keyProperty = "vacancy.id")
    void insertVacancy(@Param("vacancy") Vacancy vacancy, @Param("employerID") int employerID);

    @Select("SELECT * from vacancy WHERE id in (select vacancyID from vacancy_demand where skillID in (select id from skill where name = #{skill.name}))")
    Set<Vacancy> getVacancySetBySkill(Skill skill);

    @Select("SELECT id, postName,salary, isActive from vacancy WHERE id in (select vacancyID from vacancy_demand where skillID in (select id from skill where name = #{skillName} and `level` >= #{minLevel}))")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "demands", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.hiring.mybatis.mappers.VacancyMapper.getDemandsList", fetchType = FetchType.LAZY)),
    })
    Set<Vacancy> getVacancySetBySkillLevel(@Param("skillName") String skillName, @Param("minLevel") int minLevel);


    @Select("SELECT id, postName,salary, isActive FROM vacancy WHERE employerID = #{employerID}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "demands", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.hiring.mybatis.mappers.VacancyMapper.getDemandsList", fetchType = FetchType.LAZY)),
    })
    List<VacancyDtoResponse> getVacancyDtoListByEmployerID(@Param("employerID") int employerID);

    @Select("SELECT id, postName,salary, isActive FROM vacancy WHERE employerID = #{employerID}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "demands", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.hiring.mybatis.mappers.VacancyMapper.getDemandsList", fetchType = FetchType.LAZY)),
    })
    List<Vacancy> getListByEmployerID(@Param("employerID") int employerID);

    @Select("SELECT skill.name as skillName, skill.`level` as skillLvl, vacancy_demand.mandatory " +
            "FROM vacancy_demand, skill " +
            "WHERE vacancy_demand.vacancyID = #{vacancyID} and skill.id = vacancy_demand.skillID")
    List<DemandDtoRequest> getDemandsList(@Param("vacancyID") int vacancyID);

    @Delete("DELETE FROM vacancy")
    void clear();

    @Insert("INSERT INTO vacancy_demand ( vacancyID, skillID, mandatory) VALUES " + "( #{vacancyId}, #{skillID}, #{mandatory} )")
    @Options(useGeneratedKeys = true)
    void insertVacancyDemands(@Param("vacancyId") int vacancyId, @Param("skillID") int skillID, @Param("mandatory") int mandatory);

    @Update("UPDATE vacancy SET isActive = 1 WHERE employerID = #{id} and postName = #{vacancyName} ")
    void madeVacancyActive(@Param("vacancyName") String vacancyName, @Param("id") int id);

    @Update("UPDATE vacancy SET isActive = 0 WHERE employerID = #{id} and postName = #{vacancyName} ")
    void madeVacancyInactive(@Param("vacancyName") String vacancyName, @Param("id") int id);

    @Delete("DELETE FROM vacancy WHERE employerID = #{id} and postName = #{vacancyName} ")
    void removeVacancy(@Param("vacancyName") String vacancyName, @Param("id") int id);

    @Select("SELECT id, postName,salary, isActive FROM vacancy WHERE employerID = #{id} and isActive = 1")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "demands", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.hiring.mybatis.mappers.VacancyMapper.getDemandsList", fetchType = FetchType.LAZY)),
    })
    List<VacancyDtoResponse> getListOfActiveVacancy(@Param("id") int id);

    @Select("SELECT id, postName,salary, isActive FROM vacancy WHERE employerID = #{id} and isActive = 0")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "demands", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.hiring.mybatis.mappers.VacancyMapper.getDemandsList", fetchType = FetchType.LAZY)),
    })
    List<VacancyDtoResponse> getListOfInactiveVacancy(int id);
}
