package net.thumbtack.school.hiring.mybatis.daoimpl;


import net.thumbtack.school.hiring.dao.EmployerDao;
import net.thumbtack.school.hiring.dto.request.employer.ChangeEmployerDataRequest;
import net.thumbtack.school.hiring.dto.request.vacancy.AddVacancyDtoRequest;
import net.thumbtack.school.hiring.dto.request.vacancy.DemandDtoRequest;
import net.thumbtack.school.hiring.dto.request.vacancy.VacancyServiceDtoRequest;
import net.thumbtack.school.hiring.dto.response.modelType.VacancyDtoResponse;
import net.thumbtack.school.hiring.errors.ErrorCode;
import net.thumbtack.school.hiring.errors.ServerException;
import net.thumbtack.school.hiring.model.Employer;
import net.thumbtack.school.hiring.model.Skill;
import net.thumbtack.school.hiring.model.Vacancy;
import org.apache.ibatis.session.SqlSession;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class EmployerDaoImpl extends DaoImplBase implements EmployerDao {

    @Override
    public Employer getEmployer(String token) throws ServerException {
        try (SqlSession sqlSession = getSession()) {
            try {
                return new Employer(getEmployerMapper(sqlSession).getEmployer(token));
            } catch (RuntimeException ex) {
                throw new ServerException(ErrorCode.EMPLOYER_DONT_FOUND);
            }
        }
    }

    @Override
    public List<Employer> getAllEmployers() {
        try (SqlSession sqlSession = getSession()) {
            try {
                return getEmployerMapper(sqlSession).getAllEmployers();
            } catch (RuntimeException ex) {
                throw ex;
            }
        }
    }

    @Override
    public Employer getRegisterEmployer(String login, String password) throws ServerException {
        try (SqlSession sqlSession = getSession()) {
            try {
                Employer em = getEmployerMapper(sqlSession).getRegisterEmployer(login, password);
                if (em == null) {
                    throw new ServerException(ErrorCode.EMPLOYER_DONT_FOUND);
                }
                return em;
            } catch (RuntimeException ex) {
                throw new ServerException(ErrorCode.EMPLOYER_DONT_FOUND);
            }
        }
    }

    @Override
    public Set<Vacancy> getVacancySetBySkillLevel(String skillName, int minLevel) {
        try (SqlSession sqlSession = getSession()) {
            try {
                Set<Vacancy> set_jvstb = getVacancyMapper(sqlSession).getVacancySetBySkillLevel(skillName, minLevel);
                Set<Vacancy> set = new HashSet<>();
                for (Vacancy v : set_jvstb) {
                    set.add(new Vacancy(v));
                }
                return set;
            } catch (RuntimeException ex) {
                throw ex;
            }
        }
    }

    @Override
    public void removeVacancyFromTree(VacancyDtoResponse vacancyDto) throws ServerException {

    }

    @Override
    public void clearVacancyDataBase() {
        try (SqlSession sqlSession = getSession()) {
            try {
                getVacancyMapper(sqlSession).clear();
            } catch (RuntimeException ex) {
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public void clearEmployerDataBase() {
        try (SqlSession sqlSession = getSession()) {
            try {
                getEmployerMapper(sqlSession).clear();
            } catch (RuntimeException ex) {
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public void changeEmployerData(ChangeEmployerDataRequest employerData) throws ServerException {
        try (SqlSession sqlSession = getSession()) {
            try {
                getEmployerMapper(sqlSession).changeEmployerData(employerData);
            } catch (RuntimeException ex) {
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public void addVacancy(AddVacancyDtoRequest vacancyData, Vacancy vacancy) throws ServerException {
        try (SqlSession sqlSession = getSession()) {
            try {
                int id = getEmployer(vacancyData.getToken()).getId();
                getVacancyMapper(sqlSession).insertVacancy(vacancy, id);
                for (DemandDtoRequest d : vacancy.getDemands()) {
                    Skill s = new Skill(d.getSkill());
                    getSkillMapper(sqlSession).insertSkill(s);
                    int t = d.isMandatory() ? 1 : 0;
                    getVacancyMapper(sqlSession).insertVacancyDemands(vacancy.getId(), s.getId(), t);
                }
            } catch (RuntimeException ex) {
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public void madeVacancyActive(String token, String vacancyName) throws ServerException {
        try (SqlSession sqlSession = getSession()) {
            try {
                int id = getEmployer(token).getId();
                getVacancyMapper(sqlSession).madeVacancyActive(vacancyName, id);
            } catch (RuntimeException ex) {
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public void madeVacancyInactive(String token, String vacancyName) throws ServerException {
        try (SqlSession sqlSession = getSession()) {
            try {
                int id = getEmployer(token).getId();
                getVacancyMapper(sqlSession).madeVacancyInactive(vacancyName, id);
            } catch (RuntimeException ex) {
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public void removeVacancy(VacancyServiceDtoRequest vacancyData, VacancyDtoResponse vacancy) throws ServerException {
        try (SqlSession sqlSession = getSession()) {
            try {
                int id = getEmployer(vacancyData.getToken()).getId();
                getVacancyMapper(sqlSession).removeVacancy(vacancyData.getVacancyName(), id);
            } catch (RuntimeException ex) {
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public List<VacancyDtoResponse> getListOfActiveVacancy(String token) throws ServerException {
        try (SqlSession sqlSession = getSession()) {
            try {
                int id = getEmployer(token).getId();
                List<VacancyDtoResponse> list_hiber = getVacancyMapper(sqlSession).getListOfActiveVacancy(id);
                List<VacancyDtoResponse> list = new ArrayList<>();
                for (VacancyDtoResponse v : list_hiber) {
                    list.add(new VacancyDtoResponse(v));
                }
                return list;
            } catch (RuntimeException ex) {
                throw ex;
            }
        }
    }

    @Override
    public List<VacancyDtoResponse> getListOfInactiveVacancy(String token) throws ServerException {
        try (SqlSession sqlSession = getSession()) {
            try {
                int id = getEmployer(token).getId();
                List<VacancyDtoResponse> list_hiber = getVacancyMapper(sqlSession).getListOfInactiveVacancy(id);
                List<VacancyDtoResponse> list = new ArrayList<>();
                for (VacancyDtoResponse v : list_hiber) {
                    list.add(new VacancyDtoResponse(v));
                }
                return list;
            } catch (RuntimeException ex) {
                throw ex;
            }
        }
    }


}
