package net.thumbtack.school.hiring.mybatis.daoimpl;

import net.thumbtack.school.hiring.dao.EmployeeDao;
import net.thumbtack.school.hiring.dto.request.employee.ChangeEmployeeDataDtoRequest;
import net.thumbtack.school.hiring.dto.request.skill.ChangeSkillLevelDtoRequest;
import net.thumbtack.school.hiring.errors.ErrorCode;
import net.thumbtack.school.hiring.errors.ServerException;
import net.thumbtack.school.hiring.model.Employee;
import net.thumbtack.school.hiring.model.Skill;
import org.apache.ibatis.session.SqlSession;

import java.util.List;
import java.util.Set;

public class EmployeeDaoImpl extends DaoImplBase implements EmployeeDao {
    @Override
    public Employee getEmployee(String token) throws ServerException {
        try (SqlSession sqlSession = getSession()) {
            try {
                Employee e = getEmployeeMapper(sqlSession).getEmployee(token);
                if (e == null) {
                    throw new ServerException(ErrorCode.EMPLOYEE_DONT_FOUND);
                }
                return e;
            } catch (RuntimeException ex) {
                throw new ServerException(ErrorCode.EMPLOYEE_DONT_FOUND);
            }
        }
    }

    @Override
    public List<Employee> getAllEmployees() {
        try (SqlSession sqlSession = getSession()) {
            try {
                return getEmployeeMapper(sqlSession).getAllEmployees();
            } catch (RuntimeException ex) {
                return null;
            }
        }
    }

    @Override
    public Employee getRegisterEmployee(String login, String password) throws ServerException {
        try (SqlSession sqlSession = getSession()) {
            try {
                Employee em = getEmployeeMapper(sqlSession).getRegisterEmployee(login, password);
                if (em == null) {
                    throw new ServerException(ErrorCode.EMPLOYEE_DONT_FOUND);
                }
                return em;
            } catch (RuntimeException ex) {
                throw new ServerException(ErrorCode.EMPLOYEE_DONT_FOUND);
            }
        }
    }

    @Override
    public void addEmployeeInSortedTree(Employee employee, Skill skill) {
        try (SqlSession sqlSession = getSession()) {
            try {
                getEmployeeMapper(sqlSession).insertEmployee(employee);
                getSkillMapper(sqlSession).insertSkill(skill);
                getSkillMapper(sqlSession).addSkillToEmployee(employee.getId(), skill);
            } catch (RuntimeException ex) {
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }


    @Override
    public Set<Employee> getEmployeeSetBySkillLevel(String skillNamel, int minLevel) {
        try (SqlSession sqlSession = getSession()) {
            try {
                return getEmployeeMapper(sqlSession).getEmployeeSetBySkillLevel(skillNamel, minLevel);
            } catch (RuntimeException ex) {
                throw ex;
            }
        }
    }

    @Override
    public void clearSkillDataBase() {
        try (SqlSession sqlSession = getSession()) {
            try {
                getSkillMapper(sqlSession).clear();
            } catch (RuntimeException ex) {
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public void clearEmployeeDataBase() {
        try (SqlSession sqlSession = getSession()) {
            try {
                getEmployeeMapper(sqlSession).clear();
            } catch (RuntimeException ex) {
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public void changeEmployeeData(ChangeEmployeeDataDtoRequest employeeData) {
        try (SqlSession sqlSession = getSession()) {
            try {
                getEmployeeMapper(sqlSession).changeEmployeeData(employeeData);
            } catch (RuntimeException ex) {
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public boolean employeeIsActive(String token) throws ServerException {
        try (SqlSession sqlSession = getSession()) {
            try {
                int i = getEmployeeMapper(sqlSession).employeeIsActive(token);
                return i != 0;
            } catch (RuntimeException ex) {
                throw ex;
            }
        }
    }

    @Override
    public void madeAccountInactive(String userTokenDtoJson) throws ServerException {
        try (SqlSession sqlSession = getSession()) {
            try {
                getEmployeeMapper(sqlSession).madeAccountInactive(userTokenDtoJson);
            } catch (RuntimeException ex) {
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public void madeAccountActive(String token) throws ServerException {
        try (SqlSession sqlSession = getSession()) {
            try {
                getEmployeeMapper(sqlSession).madeAccountActive(token);
            } catch (RuntimeException ex) {
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public void addNewSkill(Skill skill, String token) throws ServerException {
        try (SqlSession sqlSession = getSession()) {
            try {
                getSkillMapper(sqlSession).insertSkill(skill);
                int id = getEmployeeMapper(sqlSession).getId(token);
                getSkillMapper(sqlSession).addSkillToEmployee(id, skill);
            } catch (RuntimeException ex) {
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public boolean containsSkill(String skillName, String token) throws ServerException {
        try (SqlSession sqlSession = getSession()) {
            try {
                int id = getEmployeeMapper(sqlSession).getId(token);
                List<Skill> list = getSkillMapper(sqlSession).getListByEmployeeID(id);
                return list.stream().map(s -> s.getName()).anyMatch(skillName::equals);
            } catch (RuntimeException ex) {
                throw ex;
            }
        }
    }

    @Override
    public void removeSkill(String token, String skillName) throws ServerException {
        try (SqlSession sqlSession = getSession()) {
            try {
                int employeeID = getEmployeeMapper(sqlSession).getId(token);
                int skillID = getSkillMapper(sqlSession).findSkillIDByEmployee(employeeID, skillName);
                getSkillMapper(sqlSession).removeSkill(skillID);
            } catch (RuntimeException ex) {
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public void changeSkillLevel(ChangeSkillLevelDtoRequest skillData) throws ServerException {
        try (SqlSession sqlSession = getSession()) {
            try {
                int employeeID = getEmployee(skillData.getToken()).getId();
                int skillID = getSkillMapper(sqlSession).findSkillIDByEmployee(employeeID, skillData.getSkillName());
                getSkillMapper(sqlSession).changeSkillLevel(skillID, skillData.getNewLevel());
            } catch (RuntimeException ex) {
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public int getSkillLevel(String token, String skillName) throws ServerException {
        try (SqlSession sqlSession = getSession()) {
            try {
                int employeeID = getEmployee(token).getId();
                int skillID = getSkillMapper(sqlSession).findSkillIDByEmployee(employeeID, skillName);
                return getSkillMapper(sqlSession).getSkillLevel(skillID);
            } catch (RuntimeException ex) {
                throw ex;
            }
        }
    }
}
