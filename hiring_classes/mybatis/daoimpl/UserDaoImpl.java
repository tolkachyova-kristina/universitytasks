package net.thumbtack.school.hiring.mybatis.daoimpl;

import net.thumbtack.school.hiring.dao.UserDao;
import net.thumbtack.school.hiring.errors.ErrorCode;
import net.thumbtack.school.hiring.errors.ServerException;
import net.thumbtack.school.hiring.model.Employee;
import net.thumbtack.school.hiring.model.Employer;
import net.thumbtack.school.hiring.model.User;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

public class UserDaoImpl extends DaoImplBase implements UserDao {
    @Override
    public void logIn(String token, User user) throws ServerException {
        try (SqlSession sqlSession = getSession()) {
            try {
                if (user.getClass() == Employee.class) {
                    getEmployeeMapper(sqlSession).logIn(token, (Employee) user);
                } else {
                    getEmployerMapper(sqlSession).logIn(token, (Employer) user);
                }
            } catch (RuntimeException ex) {
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public void logOut(String token) throws ServerException {
        try (SqlSession sqlSession = getSession()) {
            try {
                getEmployeeMapper(sqlSession).logOut(token);
                getEmployerMapper(sqlSession).logOut(token);
            } catch (RuntimeException ex) {
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public User getUser(String login, String password) throws ServerException {
        try (SqlSession sqlSession = getSession()) {
            try {

                Employer employer = getEmployerMapper(sqlSession).getRegisterEmployer(login, password);
                if (employer == null) {
                    Employee employee = getEmployeeMapper(sqlSession).getRegisterEmployee(login, password);
                    if (employee == null) {
                        return null;
                    } else {
                        return new Employee(employee);
                    }
                } else {
                    return new Employer(employer);
                }
            } catch (RuntimeException ex) {
                throw ex;
            }
        }
    }

    @Override
    public void registerUser(User user) throws ServerException {
        try (SqlSession sqlSession = getSession()) {
            try {
                if (user.getClass() == Employee.class) {
                    List<String> logins = getEmployeeMapper(sqlSession).getLoginList();
                    if (logins.contains(user.getLogin())) {
                        throw new ServerException(ErrorCode.LOGIN_ALREADY_USED);
                    }
                    getEmployeeMapper(sqlSession).insertEmployee((Employee) user);
                } else {
                    List<String> logins = getEmployerMapper(sqlSession).getLoginList();
                    if (logins.contains(user.getLogin())) {
                        throw new ServerException(ErrorCode.LOGIN_ALREADY_USED);
                    }
                    getEmployerMapper(sqlSession).registerEmployer((Employer) user);
                }
            } catch (RuntimeException ex) {
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public void removeUser(String token) throws ServerException {
        try (SqlSession sqlSession = getSession()) {
            try {
                getEmployeeMapper(sqlSession).remove(token);
                getEmployerMapper(sqlSession).remove(token);
            } catch (RuntimeException ex) {
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public boolean isLogIn(String userToken) {
        try (SqlSession sqlSession = getSession()) {
            try {
                Integer id = null;
                id = getEmployerMapper(sqlSession).getLogInEmployer(userToken);
                if (id == null) {
                    id = getEmployeeMapper(sqlSession).getLogInEmployee(userToken);
                }
                return id != null;
            } catch (RuntimeException ex) {

                throw ex;
            }

        }
    }
}
