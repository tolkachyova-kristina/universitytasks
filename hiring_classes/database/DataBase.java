package net.thumbtack.school.hiring.database;

import com.google.common.collect.TreeMultimap;
import net.thumbtack.school.hiring.dto.response.modelType.SkillDtoResponse;
import net.thumbtack.school.hiring.errors.ErrorCode;
import net.thumbtack.school.hiring.errors.ServerException;
import net.thumbtack.school.hiring.model.*;

import java.util.*;

public class DataBase {
    private static DataBase instance;
    private static Map<String, User> userMap; //login - user, все пользователи
    private static Map<String, User> loggedInUsers; //token - user, только те кто онлайн
    private static TreeMultimap<Skill, Employee> employeeTree;
    private static TreeMultimap<Skill, Vacancy> vacancyTree;

    private DataBase(String fileName) {
        loggedInUsers = new HashMap<>();
        userMap = new HashMap<>();
        employeeTree = TreeMultimap.create((o1, o2) -> {
            int nameCompareResult = o1.getName().compareTo(o2.getName());
            return nameCompareResult != 0 ? nameCompareResult : o1.getLevel() - o2.getLevel();
        }, (o1, o2) -> o1.getFirstName().compareTo(o2.getFirstName()));
        vacancyTree = TreeMultimap.create((o1, o2) -> {
            int nameCompareResult = o1.getName().compareTo(o2.getName());
            return nameCompareResult != 0 ? nameCompareResult : o1.getLevel() - o2.getLevel();
        }, (o1, o2) -> o1.getPostName().compareTo(o2.getPostName()));
    }

    public static void addEmployeeInSortedTree(Employee employee, Skill skill) {
        employeeTree.put(skill, employee);
    }

    public static Set<Employee> getEmployeeSetBySkill(Skill skill) {
        return employeeTree.get(skill);
    }

    public static void addVacancyInSortedTree(Vacancy vacancy) {
        for (SkillDtoResponse skill : vacancy.getAllSkills()) {
            vacancyTree.put(new Skill(skill), vacancy);
        }
    }

    public static Set<Vacancy> getVacancySetBySkill(Skill skill) {
        return vacancyTree.get(skill);
    }

    public static DataBase getInstance(String fileName) { //
        if (instance == null) {        //если объект еще не создан
            instance = new DataBase(fileName);    //создать новый объект
        }
        return instance;        // вернуть ранее созданный объект
    }

    //добавить нового пользователя в базу
    public static void registerUser(User user) throws ServerException {
        User check = userMap.putIfAbsent(user.getLogin(), user);
        if (check != null) {
            throw new ServerException(ErrorCode.LOGIN_ALREADY_USED);
        }
    }

    //взять зарег. пользователя
    public static User getOneUserOfAll(String login, String password) throws ServerException {
        User user = userMap.get(login);
        if (user == null) {
            throw new ServerException(ErrorCode.USER_DONT_FOUND);
        }
        return user;
    }

    //взять онлайн пользователя
    public static User getLogInUser(String token) throws ServerException {
        User user = loggedInUsers.get(token);
        if (user == null) {
            throw new ServerException(ErrorCode.USER_DONT_FOUND);
        }
        return user;
    }

    //удалить пользователя
    public static void removeUser(String token) {
        userMap.remove(loggedInUsers.get(token).getLogin());
        loggedInUsers.remove(token);
    }

    //получить список всех работников
    public static List<Employee> getAllEmployee() {
        List<Employee> result = new ArrayList<>();
        for (User elem : userMap.values()) {
            if (elem.getClass() == Employee.class) {
                result.add((Employee) elem);
            }
        }
        return result;
    }

    //получить список всех работодателей
    public static List<Employer> getAllEmployer() {
        List<Employer> result = new ArrayList<>();
        for (User elem : userMap.values()) {
            if (elem.getClass() == Employer.class) {
                result.add((Employer) elem);
            }
        }
        return result;
    }

    //польз.ль залогиниться
    public static void userLogIn(String token, User user) {
        loggedInUsers.put(token, user);
    }

    //польз.ль разлогиниться
    public static void userLogOut(String token) throws ServerException {

        loggedInUsers.remove(token);
    }

    public static boolean userIsLogIn(String userToken) {
        return loggedInUsers.containsKey(userToken);
    }

    public static void getDown() {
        userMap.clear();
        loggedInUsers.clear();
        employeeTree.clear();
        vacancyTree.clear();
    }
}
