package net.thumbtack.school.hiring;

import com.google.gson.Gson;
import net.thumbtack.school.hiring.dto.request.employee.ChangeEmployeeDataDtoRequest;
import net.thumbtack.school.hiring.dto.request.employee.RegisterEmployeeDtoRequest;
import net.thumbtack.school.hiring.dto.request.user.LogInDtoRequest;
import net.thumbtack.school.hiring.dto.request.user.TokenDtoRequest;
import net.thumbtack.school.hiring.dto.response.baseType.BooleanDtoResponse;
import net.thumbtack.school.hiring.dto.response.modelType.EmployeeDataDtoResponse;
import net.thumbtack.school.hiring.dto.response.serviceType.ErrorDtoResponse;
import net.thumbtack.school.hiring.errors.ServerException;
import net.thumbtack.school.hiring.utils.Mode;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;

public class EmployeeTests {

    @BeforeEach
    public void testStart() throws ServerException {
        Server.startServer("", Mode.SQL);
    }

    @AfterEach
    public void testStop() throws ServerException {
        Server.stopServer();
    }

    @Test
    public void testRegisterEmployee() {
        Gson gson = new Gson();
        RegisterEmployeeDtoRequest employeeDtoRequest = new RegisterEmployeeDtoRequest("Petrov", "ptr20202", "123", "email@gmail.com");
        String employeeTokenDto = Server.registerEmployee(gson.toJson(employeeDtoRequest));
        BooleanDtoResponse bool = gson.fromJson(Server.containsEmployee(gson.toJson(employeeDtoRequest)), BooleanDtoResponse.class);
        assertEquals(true, bool.isValue());
        bool = gson.fromJson(Server.isLogIn(employeeTokenDto), BooleanDtoResponse.class);
        assertEquals(true, bool.isValue());

        RegisterEmployeeDtoRequest controlDtoRequest = new RegisterEmployeeDtoRequest("Sidiriv", "ptr20202", "123", "email@gmail.com");
        assertEquals("LOGIN_ALREADY_USED", gson.fromJson(Server.registerEmployee(gson.toJson(controlDtoRequest)), ErrorDtoResponse.class).getError());
    }

    @Test
    public void testWrongDataRegisterEmployee() {

        Gson gson = new Gson();
        ErrorDtoResponse error = gson.fromJson(Server.registerEmployee(null), ErrorDtoResponse.class);
        assertEquals("NULL_INPUT_STRING", error.getError());
        error = gson.fromJson(Server.registerEmployee(""), ErrorDtoResponse.class);
        assertEquals("WRONG_INPUT_DATA", error.getError());
        error = gson.fromJson(Server.registerEmployee("ADSZFXGCHJVKBL.;"), ErrorDtoResponse.class);
        assertEquals("WRONG_INPUT_DATA", error.getError());

        error = gson.fromJson(Server.registerEmployee(gson.toJson(new RegisterEmployeeDtoRequest("", "ptr20202", "123", "email@gmail.com"))), ErrorDtoResponse.class);
        assertEquals("WRONG_NAME", error.getError());
        error = gson.fromJson(Server.registerEmployee(gson.toJson(new RegisterEmployeeDtoRequest("ptr20202", "", "123", "email@gmail.com"))), ErrorDtoResponse.class);
        assertEquals("WRONG_LOGIN", error.getError());
        error = gson.fromJson(Server.registerEmployee(gson.toJson(new RegisterEmployeeDtoRequest("ывфывфы", "ptr20202", "", "email@gmail.com"))), ErrorDtoResponse.class);
        assertEquals("WRONG_PASSWORD", error.getError());
        error = gson.fromJson(Server.registerEmployee(gson.toJson(new RegisterEmployeeDtoRequest("ывфывфы", "ptr20202", "dffsdfs", ""))), ErrorDtoResponse.class);
        assertEquals("WRONG_EMAIL", error.getError());

        error = gson.fromJson(Server.registerEmployee(gson.toJson(new RegisterEmployeeDtoRequest(null, "", "", ""))), ErrorDtoResponse.class);
        assertEquals("WRONG_NAME", error.getError());
        error = gson.fromJson(Server.registerEmployee(gson.toJson(new RegisterEmployeeDtoRequest("ptr20202", null, " ", ""))), ErrorDtoResponse.class);
        assertEquals("WRONG_LOGIN", error.getError());
        error = gson.fromJson(Server.registerEmployee(gson.toJson(new RegisterEmployeeDtoRequest("ывфывфы", "ptr20202", null, ""))), ErrorDtoResponse.class);
        assertEquals("WRONG_PASSWORD", error.getError());
        error = gson.fromJson(Server.registerEmployee(gson.toJson(new RegisterEmployeeDtoRequest("ывфывфы", "ptr20202", "dffsdfs", null))), ErrorDtoResponse.class);
        assertEquals("WRONG_EMAIL", error.getError());


    }

    @Test
    public void testChangeEmployee() {

        Gson gson = new Gson();
        RegisterEmployeeDtoRequest em1 = new RegisterEmployeeDtoRequest("Petrov", "ptr20202", "123", "email@gmail.com");
        String employeeToken = gson.fromJson(Server.registerEmployee(gson.toJson(em1)), TokenDtoRequest.class).getToken();

        ChangeEmployeeDataDtoRequest newEmployeeData = new ChangeEmployeeDataDtoRequest(employeeToken, "ABC", "EFG", "0123");
        Server.changeEmployeeData(gson.toJson(newEmployeeData));

        Server.logOut(gson.toJson(new TokenDtoRequest(employeeToken)));
        String newToken = gson.fromJson(Server.logIn(gson.toJson(new LogInDtoRequest("ptr20202", "0123"))), TokenDtoRequest.class).getToken();
        String string = Server.getEmployeeData(gson.toJson(new TokenDtoRequest(newToken)));
        EmployeeDataDtoResponse employeeData = gson.fromJson(string, EmployeeDataDtoResponse.class);
        assertEquals("ABC", employeeData.getName());
        assertEquals("EFG", employeeData.getEmail());


    }

    @Test
    public void testWrongChangeEmployee() {

        Gson gson = new Gson();
        RegisterEmployeeDtoRequest employeeDtoRequest = new RegisterEmployeeDtoRequest("Petrov", "ptr20202", "123", "email@gmail.com");
        String employeeTokenDto = Server.registerEmployee(gson.toJson(employeeDtoRequest));

        ErrorDtoResponse error = gson.fromJson(Server.changeEmployeeData(gson.toJson(new ChangeEmployeeDataDtoRequest(employeeTokenDto, "", "EFG", "0123"))), ErrorDtoResponse.class);
        assertEquals("WRONG_NAME", error.getError());
        error = gson.fromJson(Server.changeEmployeeData(gson.toJson(new ChangeEmployeeDataDtoRequest(employeeTokenDto, "dfsdf", "", "0123"))), ErrorDtoResponse.class);
        assertEquals("WRONG_EMAIL", error.getError());
        error = gson.fromJson(Server.changeEmployeeData(gson.toJson(new ChangeEmployeeDataDtoRequest(employeeTokenDto, "asdfaf", "EFG", ""))), ErrorDtoResponse.class);
        assertEquals("WRONG_PASSWORD", error.getError());

        String string = Server.getEmployeeData(employeeTokenDto);
        EmployeeDataDtoResponse employeeData = gson.fromJson(string, EmployeeDataDtoResponse.class);
        assertEquals("Petrov", employeeData.getName());
        assertEquals("email@gmail.com", employeeData.getEmail());

    }

    @Test
    public void testEmployeeIsActive() {

        Gson gson = new Gson();
        RegisterEmployeeDtoRequest employeeDtoRequest = new RegisterEmployeeDtoRequest("Petrov", "ptr20202", "123", "email@gmail.com");
        String employeeTokenDto = Server.registerEmployee(gson.toJson(employeeDtoRequest));

        BooleanDtoResponse bool = gson.fromJson(Server.employeeIsActive(employeeTokenDto), BooleanDtoResponse.class);
        assertEquals(true, bool.isValue());

        Server.madeAccountInactive(employeeTokenDto);
        bool = gson.fromJson(Server.employeeIsActive(employeeTokenDto), BooleanDtoResponse.class);
        assertEquals(false, bool.isValue());

        Server.madeAccountActive(employeeTokenDto);
        bool = gson.fromJson(Server.employeeIsActive(employeeTokenDto), BooleanDtoResponse.class);
        assertEquals(true, bool.isValue());


    }
}
