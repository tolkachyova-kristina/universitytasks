package net.thumbtack.school.hiring;

import com.google.gson.Gson;
import net.thumbtack.school.hiring.dto.request.employee.RegisterEmployeeDtoRequest;
import net.thumbtack.school.hiring.dto.request.skill.AddNewSkillDtoRequest;
import net.thumbtack.school.hiring.dto.request.skill.ChangeSkillLevelDtoRequest;
import net.thumbtack.school.hiring.dto.request.skill.SkillServiceDtoRequest;
import net.thumbtack.school.hiring.dto.request.user.TokenDtoRequest;
import net.thumbtack.school.hiring.dto.response.baseType.BooleanDtoResponse;
import net.thumbtack.school.hiring.dto.response.baseType.IntegerDtoResponse;
import net.thumbtack.school.hiring.dto.response.serviceType.ErrorDtoResponse;
import net.thumbtack.school.hiring.errors.ServerException;
import net.thumbtack.school.hiring.utils.Mode;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;

public class SkillTest {
    final Gson gson = new Gson();

    @BeforeEach
    public void testStart() throws ServerException {
        Server.startServer("", Mode.SQL);
    }

    @AfterEach
    public void testStop() throws ServerException {
        Server.stopServer();
    }

    @Test
    public void testAddSkill() {


        RegisterEmployeeDtoRequest employeeDtoRequest = new RegisterEmployeeDtoRequest("Petrov", "ptr20202", "123", "email@gmail.com");
        String employeeToken = gson.fromJson(Server.registerEmployee(gson.toJson(employeeDtoRequest)), TokenDtoRequest.class).getToken();

        Server.addNewSkill(gson.toJson(new AddNewSkillDtoRequest(employeeToken, "html", 3)));
        Server.containsSkill(gson.toJson(new SkillServiceDtoRequest(employeeToken, "html")));

    }

    @Test
    public void testWrongAddSkill() {


        RegisterEmployeeDtoRequest employeeDtoRequest = new RegisterEmployeeDtoRequest("Petrov", "ptr20202", "123", "email@gmail.com");
        String employeeToken = Server.registerEmployee(gson.toJson(employeeDtoRequest));

        ErrorDtoResponse error = gson.fromJson(Server.addNewSkill(null), ErrorDtoResponse.class);

        assertEquals("NULL_INPUT_STRING", error.getError());
        error = gson.fromJson(Server.addNewSkill(""), ErrorDtoResponse.class);
        assertEquals("WRONG_INPUT_DATA", error.getError());
        error = gson.fromJson(Server.addNewSkill("NULL"), ErrorDtoResponse.class);
        assertEquals("WRONG_INPUT_DATA", error.getError());

    }

    @Test
    public void testRemoveSkill() {

        RegisterEmployeeDtoRequest employeeDtoRequest = new RegisterEmployeeDtoRequest("Petrov", "ptr20202", "123", "email@gmail.com");
        String employeeToken = gson.fromJson(Server.registerEmployee(this.gson.toJson(employeeDtoRequest)), TokenDtoRequest.class).getToken();

        Server.addNewSkill(this.gson.toJson(new AddNewSkillDtoRequest(employeeToken, "html", 3)));
        Server.addNewSkill(this.gson.toJson(new AddNewSkillDtoRequest(employeeToken, "java", 3)));
        Server.removeSkill(this.gson.toJson(new SkillServiceDtoRequest(employeeToken, "html")));
        BooleanDtoResponse bool = gson.fromJson(Server.containsSkill(this.gson.toJson(new SkillServiceDtoRequest(employeeToken, "html"))), BooleanDtoResponse.class);
        assertEquals(false, bool.isValue());
        bool = gson.fromJson(Server.containsSkill(this.gson.toJson(new SkillServiceDtoRequest(employeeToken, "java"))), BooleanDtoResponse.class);
        assertEquals(true, bool.isValue());

    }

    @Test
    public void testChangeSkillLevel() {

        RegisterEmployeeDtoRequest employeeDtoRequest = new RegisterEmployeeDtoRequest("Petrov", "ptr20202", "123", "email@gmail.com");
        String employeeToken = gson.fromJson(Server.registerEmployee(gson.toJson(employeeDtoRequest)), TokenDtoRequest.class).getToken();

        Server.addNewSkill(this.gson.toJson(new AddNewSkillDtoRequest(employeeToken, "html", 3)));
        Server.changeSkillLevel(this.gson.toJson(new ChangeSkillLevelDtoRequest(employeeToken, "html", 5)));
        IntegerDtoResponse value = gson.fromJson(Server.getSkillLevel(this.gson.toJson(new SkillServiceDtoRequest(employeeToken, "html"))), IntegerDtoResponse.class);
        assertEquals(5, value.getValue());

    }

    @Test
    public void testWrongChangeSkillLevel() {

        RegisterEmployeeDtoRequest employeeDtoRequest = new RegisterEmployeeDtoRequest("Petrov", "ptr20202", "123", "email@gmail.com");
        String employeeToken = gson.fromJson(Server.registerEmployee(gson.toJson(employeeDtoRequest)), TokenDtoRequest.class).getToken();
        Server.addNewSkill(this.gson.toJson(new AddNewSkillDtoRequest(employeeToken, "html", 3)));

        ErrorDtoResponse error = gson.fromJson(Server.changeSkillLevel(this.gson.toJson(new ChangeSkillLevelDtoRequest(employeeToken, "html", -1))), ErrorDtoResponse.class);
        assertEquals("WRONG_SKILL_LEVEL", error.getError());
        error = gson.fromJson(Server.changeSkillLevel(this.gson.toJson(new ChangeSkillLevelDtoRequest(employeeToken, "html", 29013))), ErrorDtoResponse.class);
        assertEquals("WRONG_SKILL_LEVEL", error.getError());
        error = gson.fromJson(Server.changeSkillLevel(this.gson.toJson(new ChangeSkillLevelDtoRequest("employeeToken", "css", 3))), ErrorDtoResponse.class);
        assertEquals("EMPLOYEE_DONT_FOUND", error.getError());

    }
}