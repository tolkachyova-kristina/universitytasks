package net.thumbtack.school.hiring;

import com.google.gson.Gson;
import net.thumbtack.school.hiring.dto.request.employee.RegisterEmployeeDtoRequest;
import net.thumbtack.school.hiring.dto.request.employer.RegisterEmployerDtoRequest;
import net.thumbtack.school.hiring.dto.request.skill.AddNewSkillDtoRequest;
import net.thumbtack.school.hiring.dto.request.user.TokenDtoRequest;
import net.thumbtack.school.hiring.dto.request.vacancy.AddVacancyDtoRequest;
import net.thumbtack.school.hiring.dto.request.vacancy.VacancyServiceDtoRequest;
import net.thumbtack.school.hiring.dto.response.modelType.VacancyDtoResponse;
import net.thumbtack.school.hiring.errors.ServerException;
import net.thumbtack.school.hiring.utils.Mode;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestGetVacancyListByEmployee {
    final static Gson gson = new Gson();
    private static String employeeToken;
    private static String employerToken;

    @BeforeAll
    public static void testStart() throws ServerException {
        Server.startServer("", Mode.SQL);
        RegisterEmployerDtoRequest employerDtoRequest = new RegisterEmployerDtoRequest("Ivanov55", "1234560", "ivanov", "ABCGame", "Address, 1/2", "email@gmail.com");
        employerToken = gson.fromJson(Server.registerEmployer(gson.toJson(employerDtoRequest)), TokenDtoRequest.class).getToken();

        AddVacancyDtoRequest webDeveloper = new AddVacancyDtoRequest(employerToken, "Web Developer", 1500);
        webDeveloper.addDemand("html", 3, true);
        webDeveloper.addDemand("css", 2, false);
        webDeveloper.addDemand("java-script", 1, true);
        Server.addVacancy(gson.toJson(webDeveloper));

        AddVacancyDtoRequest mainWebDeveloper = new AddVacancyDtoRequest(employerToken, "Main Web Developer", 3500);
        mainWebDeveloper.addDemand("html", 5, true);
        mainWebDeveloper.addDemand("css", 3, false);
        mainWebDeveloper.addDemand("java-script", 5, true);
        Server.addVacancy(gson.toJson(mainWebDeveloper));

        AddVacancyDtoRequest developerOne = new AddVacancyDtoRequest(employerToken, "Developer One", 3500);
        developerOne.addDemand("html", 3, true);
        developerOne.addDemand("css", 3, true);
        developerOne.addDemand("java-script", 5, false);
        developerOne.addDemand("java", 5, false);
        Server.addVacancy(gson.toJson(developerOne));

        AddVacancyDtoRequest developerTwo = new AddVacancyDtoRequest(employerToken, "Developer TwO", 3500);
        developerTwo.addDemand("html", 3, true);
        developerTwo.addDemand("css", 3, true);
        developerTwo.addDemand("java-script", 2, true);
        developerTwo.addDemand("java", 3, false);
        Server.addVacancy(gson.toJson(developerTwo));


        RegisterEmployeeDtoRequest employeeDtoRequest = new RegisterEmployeeDtoRequest("Petrov", "1@mail.ru", "petr11", "1234");
        employeeToken = gson.fromJson(Server.registerEmployee(gson.toJson(employeeDtoRequest)), TokenDtoRequest.class).getToken();

        Server.addNewSkill(gson.toJson(new AddNewSkillDtoRequest(employeeToken, "html", 3)));
        Server.addNewSkill(gson.toJson(new AddNewSkillDtoRequest(employeeToken, "css", 3)));
        Server.addNewSkill(gson.toJson(new AddNewSkillDtoRequest(employeeToken, "java-script", 3)));
        Server.addNewSkill(gson.toJson(new AddNewSkillDtoRequest(employeeToken, "java", 3)));
    }

    @AfterAll
    public static void testStop() {
        Server.stopServer();
    }

    @Test
    public void testGetListOfAllVacanciesMeetsTheRequirementsOfTheEmployerAtANeededLvl() {
        List<VacancyDtoResponse> control = new ArrayList<>();
        VacancyDtoResponse vacancyDto = gson.fromJson(Server.getVacancy(gson.toJson(new VacancyServiceDtoRequest(employerToken, "Developer TwO"))), VacancyDtoResponse.class);
        control.add(vacancyDto);
        vacancyDto = gson.fromJson(Server.getVacancy(gson.toJson(new VacancyServiceDtoRequest(employerToken, "Web Developer"))), VacancyDtoResponse.class);
        control.add(vacancyDto);
        assertEquals(gson.toJson(control), Server.getListOfAllVacanciesMeetsTheRequirementsOfTheEmployerAtANeededLvl(gson.toJson(new TokenDtoRequest(employeeToken))));

    }

    @Test
    public void testGetListOfAllVacanciesMeetsTheRequirementsOfTheEmployerOnAnyLvl() {
        List<VacancyDtoResponse> control = new ArrayList<>();
        VacancyDtoResponse vacancyDto = gson.fromJson(Server.getVacancy(gson.toJson(new VacancyServiceDtoRequest(employerToken, "Developer One"))), VacancyDtoResponse.class);
        control.add(vacancyDto);
        vacancyDto = gson.fromJson(Server.getVacancy(gson.toJson(new VacancyServiceDtoRequest(employerToken, "Developer TwO"))), VacancyDtoResponse.class);
        control.add(vacancyDto);
        vacancyDto = gson.fromJson(Server.getVacancy(gson.toJson(new VacancyServiceDtoRequest(employerToken, "Main Web Developer"))), VacancyDtoResponse.class);
        control.add(vacancyDto);
        vacancyDto = gson.fromJson(Server.getVacancy(gson.toJson(new VacancyServiceDtoRequest(employerToken, "Web Developer"))), VacancyDtoResponse.class);
        control.add(vacancyDto);


        assertEquals(gson.toJson(control), Server.getListOfAllVacanciesMeetsTheRequirementsOfTheEmployerOnAnyLvl(gson.toJson(new TokenDtoRequest(employeeToken))));

    }

    @Test
    public void testGetListOfAllVacanciesMeetsTheBindingRequirementsOfTheEmployerAtANeededLvl() {
        List<VacancyDtoResponse> control = new ArrayList<>();
        VacancyDtoResponse vacancyDto = gson.fromJson(Server.getVacancy(gson.toJson(new VacancyServiceDtoRequest(employerToken, "Developer One"))), VacancyDtoResponse.class);
        control.add(vacancyDto);
        vacancyDto = gson.fromJson(Server.getVacancy(gson.toJson(new VacancyServiceDtoRequest(employerToken, "Developer TwO"))), VacancyDtoResponse.class);
        control.add(vacancyDto);
        vacancyDto = gson.fromJson(Server.getVacancy(gson.toJson(new VacancyServiceDtoRequest(employerToken, "Web Developer"))), VacancyDtoResponse.class);
        control.add(vacancyDto);

        assertEquals(gson.toJson(control), Server.getListOfAllVacanciesMeetsTheBindingRequirementsOfTheEmployerAtANeededLvl(gson.toJson(new TokenDtoRequest(employeeToken))));
    }

    @Test
    public void testGetListOfAllVacanciesMeetsAtLeastOneOfTheRequirementsOfTheEmployerAtNeededLvl() {
        List<VacancyDtoResponse> control = new ArrayList<>();
        VacancyDtoResponse vacancyDto = gson.fromJson(Server.getVacancy(gson.toJson(new VacancyServiceDtoRequest(employerToken, "Developer TwO"))), VacancyDtoResponse.class);
        control.add(vacancyDto);
        vacancyDto = gson.fromJson(Server.getVacancy(gson.toJson(new VacancyServiceDtoRequest(employerToken, "Web Developer"))), VacancyDtoResponse.class);
        control.add(vacancyDto);
        vacancyDto = gson.fromJson(Server.getVacancy(gson.toJson(new VacancyServiceDtoRequest(employerToken, "Developer One"))), VacancyDtoResponse.class);
        control.add(vacancyDto);
        vacancyDto = gson.fromJson(Server.getVacancy(gson.toJson(new VacancyServiceDtoRequest(employerToken, "Main Web Developer"))), VacancyDtoResponse.class);
        control.add(vacancyDto);

        assertEquals(gson.toJson(control), Server.getListOfAllVacanciesMeetsAtLeastOneOfTheRequirementsOfTheEmployerAtNeededLvl(gson.toJson(new TokenDtoRequest(employeeToken))));

    }

}
