package net.thumbtack.school.hiring;

import com.google.gson.Gson;
import net.thumbtack.school.hiring.dto.request.employee.RegisterEmployeeDtoRequest;
import net.thumbtack.school.hiring.dto.request.user.LogInDtoRequest;
import net.thumbtack.school.hiring.dto.response.baseType.BooleanDtoResponse;
import net.thumbtack.school.hiring.dto.response.serviceType.ErrorDtoResponse;
import net.thumbtack.school.hiring.errors.ServerException;
import net.thumbtack.school.hiring.utils.Mode;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;

public class UserTest {
    final Gson gson = new Gson();

    @BeforeEach
    public void testStart() throws ServerException {
        Server.startServer("", Mode.SQL);
    }

    @AfterEach
    public void testStop() throws ServerException {
        Server.stopServer();
    }

    @Test
    public void testLogInLogOut() {
        RegisterEmployeeDtoRequest employeeDto = new RegisterEmployeeDtoRequest("Petrov", "ptr20202", "123", "email@gmail.com");
        String tokenEmployeeDto = Server.registerEmployee(gson.toJson(employeeDto));

        BooleanDtoResponse bool = gson.fromJson(Server.isLogIn(tokenEmployeeDto), BooleanDtoResponse.class);
        assertEquals(true, bool.isValue());
        Server.logOut(tokenEmployeeDto);
        bool = gson.fromJson(Server.isLogIn(tokenEmployeeDto), BooleanDtoResponse.class);
        assertEquals(false, bool.isValue());

        String newTokenEmployeeDto = Server.logIn(gson.toJson(new LogInDtoRequest("ptr20202", "123")));
        bool = gson.fromJson(Server.isLogIn(tokenEmployeeDto), BooleanDtoResponse.class);
        assertEquals(false, bool.isValue());
        bool = gson.fromJson(Server.isLogIn(newTokenEmployeeDto), BooleanDtoResponse.class);
        assertEquals(true, bool.isValue());
    }

    @Test
    public void testLogInLogOutWithException() {
        ErrorDtoResponse error = gson.fromJson(Server.logIn(null), ErrorDtoResponse.class);
        assertEquals("NULL_INPUT_STRING", error.getError());
        error = gson.fromJson(Server.logIn(""), ErrorDtoResponse.class);
        assertEquals("WRONG_INPUT_DATA", error.getError());
        error = gson.fromJson(Server.logIn("null"), ErrorDtoResponse.class);
        assertEquals("WRONG_INPUT_DATA", error.getError());

        error = gson.fromJson(Server.logOut(null), ErrorDtoResponse.class);
        assertEquals("NULL_INPUT_STRING", error.getError());
        error = gson.fromJson(Server.logOut(""), ErrorDtoResponse.class);
        assertEquals("WRONG_INPUT_DATA", error.getError());
        error = gson.fromJson(Server.logOut("null"), ErrorDtoResponse.class);
        assertEquals("WRONG_INPUT_DATA", error.getError());
    }

    @Test
    public void testRemove() {
        RegisterEmployeeDtoRequest employeeDto = new RegisterEmployeeDtoRequest("Petrov", "ptr20202", "123", "email@gmail.com");
        String tokenEmployeeDto = Server.registerEmployee(gson.toJson(employeeDto));
        RegisterEmployeeDtoRequest control = new RegisterEmployeeDtoRequest("Petrov", "ptr20202", "123", "email@gmail.com");

        BooleanDtoResponse bool = gson.fromJson(Server.containsEmployee(gson.toJson(control)), BooleanDtoResponse.class);
        assertEquals(true, bool.isValue());
        Server.removeUser(tokenEmployeeDto);

        ErrorDtoResponse error = gson.fromJson(Server.containsEmployee(gson.toJson(control)), ErrorDtoResponse.class);
        assertEquals("EMPLOYEE_DONT_FOUND", error.getError());
    }

}
