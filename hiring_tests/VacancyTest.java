package net.thumbtack.school.hiring;

import com.google.gson.Gson;
import net.thumbtack.school.hiring.dto.request.employer.RegisterEmployerDtoRequest;
import net.thumbtack.school.hiring.dto.request.user.TokenDtoRequest;
import net.thumbtack.school.hiring.dto.request.vacancy.AddVacancyDtoRequest;
import net.thumbtack.school.hiring.dto.request.vacancy.VacancyServiceDtoRequest;
import net.thumbtack.school.hiring.dto.response.baseType.BooleanDtoResponse;
import net.thumbtack.school.hiring.errors.ServerException;
import net.thumbtack.school.hiring.utils.Mode;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class VacancyTest {
    final Gson gson = new Gson();

    @BeforeEach
    public void testStart() throws ServerException {
        Server.startServer("", Mode.SQL);
    }

    @AfterEach
    public void testStop() throws ServerException {
        Server.stopServer();
    }

    @Test
    public void testAddVacancy() throws ServerException {

        RegisterEmployerDtoRequest employerDto = new RegisterEmployerDtoRequest("Ivanov55", "ivanov", "123", "email@gmail.com", "ABCGame", "Address, 1/2");
        String tokenEmployer = this.gson.fromJson(Server.registerEmployer(gson.toJson(employerDto)), TokenDtoRequest.class).getToken();

        AddVacancyDtoRequest v = new AddVacancyDtoRequest(tokenEmployer, "name", 200);
        v.addDemand("html", 3, true);
        v.addDemand("css", 4, false);
        v.addDemand("java-script", 4, true);

        Server.addVacancy(this.gson.toJson(v));
        BooleanDtoResponse bool = gson.fromJson(Server.containsVacancy(this.gson.toJson(new VacancyServiceDtoRequest(tokenEmployer, "name"))), BooleanDtoResponse.class);
        assertEquals(true, bool.isValue());

    }

    @Test
    public void testVacancyIsActive() {

        RegisterEmployerDtoRequest employerDto = new RegisterEmployerDtoRequest("Ivanov55", "ivanov", "123", "email@gmail.com", "ABCGame", "Address, 1/2");
        String tokenEmployer = this.gson.fromJson(Server.registerEmployer(gson.toJson(employerDto)), TokenDtoRequest.class).getToken();
        Server.addVacancy(this.gson.toJson(new AddVacancyDtoRequest(tokenEmployer, "name", 200)));

        BooleanDtoResponse bool = gson.fromJson(Server.vacancyIsActive(this.gson.toJson(new VacancyServiceDtoRequest(tokenEmployer, "name"))), BooleanDtoResponse.class);
        assertEquals(true, bool.isValue());

        Server.madeVacancyInactive(this.gson.toJson(new VacancyServiceDtoRequest(tokenEmployer, "name")));
        bool = gson.fromJson(Server.vacancyIsActive(this.gson.toJson(new VacancyServiceDtoRequest(tokenEmployer, "name"))), BooleanDtoResponse.class);

        assertEquals(false, bool.isValue());

        Server.madeVacancyActive(this.gson.toJson(new VacancyServiceDtoRequest(tokenEmployer, "name")));
        bool = gson.fromJson(Server.vacancyIsActive(this.gson.toJson(new VacancyServiceDtoRequest(tokenEmployer, "name"))), BooleanDtoResponse.class);

        assertEquals(true, bool.isValue());

    }

    @Test
    public void testRemoveVacancy() {

        RegisterEmployerDtoRequest employerDto = new RegisterEmployerDtoRequest("Ivanov55", "ivanov", "123", "email@gmail.com", "ABCGame", "Address, 1/2");
        String tokenEmployer = this.gson.fromJson(Server.registerEmployer(gson.toJson(employerDto)), TokenDtoRequest.class).getToken();
        Server.addVacancy(this.gson.toJson(new AddVacancyDtoRequest(tokenEmployer, "name", 200)));

        Server.removeVacancy(this.gson.toJson(new VacancyServiceDtoRequest(tokenEmployer, "name")));
        BooleanDtoResponse bool = gson.fromJson(Server.containsVacancy(this.gson.toJson(new VacancyServiceDtoRequest(tokenEmployer, "name"))), BooleanDtoResponse.class);

        assertEquals(false, bool.isValue());

    }

    @Test
    public void testVacancyActiveAndInactiveList() {

        RegisterEmployerDtoRequest employerDto = new RegisterEmployerDtoRequest("Ivanov55", "ivanov", "123", "email@gmail.com", "ABCGame", "Address, 1/2");
        String tokenEmployer = this.gson.fromJson(Server.registerEmployer(gson.toJson(employerDto)), TokenDtoRequest.class).getToken();

        Server.addVacancy(this.gson.toJson(new AddVacancyDtoRequest(tokenEmployer, "first", 200)));
        Server.addVacancy(this.gson.toJson(new AddVacancyDtoRequest(tokenEmployer, "second", 200)));
        Server.addVacancy(this.gson.toJson(new AddVacancyDtoRequest(tokenEmployer, "third", 200)));

        Server.madeVacancyInactive(this.gson.toJson(new VacancyServiceDtoRequest(tokenEmployer, "second")));
        List<String> active = new ArrayList<>();
        active.add(Server.getVacancy(this.gson.toJson(new VacancyServiceDtoRequest(tokenEmployer, "first"))));
        active.add(Server.getVacancy(this.gson.toJson(new VacancyServiceDtoRequest(tokenEmployer, "third"))));

        List<String> inactive = new ArrayList<>();
        inactive.add(Server.getVacancy(this.gson.toJson(new VacancyServiceDtoRequest(tokenEmployer, "second"))));

        assertEquals(active.toString().replaceAll("\\s", ""), Server.getListOfActiveVacancy(this.gson.toJson(new TokenDtoRequest(tokenEmployer))));
        assertEquals(inactive.toString().replaceAll("\\s", ""), Server.getListOfInactiveVacancy(this.gson.toJson(new TokenDtoRequest(tokenEmployer))));

    }

}
