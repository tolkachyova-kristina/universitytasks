package net.thumbtack.school.hiring;


import com.google.gson.Gson;
import net.thumbtack.school.hiring.dto.request.employer.ChangeEmployerDataRequest;
import net.thumbtack.school.hiring.dto.request.employer.RegisterEmployerDtoRequest;
import net.thumbtack.school.hiring.dto.request.user.LogInDtoRequest;
import net.thumbtack.school.hiring.dto.request.user.TokenDtoRequest;
import net.thumbtack.school.hiring.dto.response.baseType.BooleanDtoResponse;
import net.thumbtack.school.hiring.dto.response.modelType.EmployerDataResponse;
import net.thumbtack.school.hiring.dto.response.serviceType.ErrorDtoResponse;
import net.thumbtack.school.hiring.errors.ServerException;
import net.thumbtack.school.hiring.utils.Mode;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;

public class EmployerTests {
    @BeforeEach
    public void testStart() throws ServerException {
        Server.startServer("", Mode.SQL);
    }

    @AfterEach
    public void testStop() throws ServerException {
        Server.stopServer();
    }

    @Test
    public void testRegisterEmployer() {

        Gson gson = new Gson();
        RegisterEmployerDtoRequest employerDto = new RegisterEmployerDtoRequest("Ivanov55", "123", "ivanov", "ABCGame", "Address, 1/2", "email@gmail.com");
        String tokenEmployerDto = Server.registerEmployer(gson.toJson(employerDto));

        BooleanDtoResponse bool = gson.fromJson(Server.containsEmployer(gson.toJson(employerDto)), BooleanDtoResponse.class);
        assertEquals(true, bool.isValue());
        bool = gson.fromJson(Server.isLogIn(tokenEmployerDto), BooleanDtoResponse.class);
        assertEquals(true, bool.isValue());

        RegisterEmployerDtoRequest controlDto = new RegisterEmployerDtoRequest("Ivanov55", "123", "ivanov", "ABCGame", "Address, 1/2", "email@gmail.com");
        assertEquals("LOGIN_ALREADY_USED", gson.fromJson(Server.registerEmployer(gson.toJson(controlDto)), ErrorDtoResponse.class).getError());

    }

    @Test
    public void testRegisterEmployerWithException() {

        Gson gson = new Gson();

        ErrorDtoResponse error = gson.fromJson(Server.registerEmployer(null), ErrorDtoResponse.class);
        assertEquals("NULL_INPUT_STRING", error.getError());
        error = gson.fromJson(Server.registerEmployer(""), ErrorDtoResponse.class);
        assertEquals("WRONG_INPUT_DATA", error.getError());
        error = gson.fromJson(Server.registerEmployer("ADSZFXGCHJVKBL.;"), ErrorDtoResponse.class);
        assertEquals("WRONG_INPUT_DATA", error.getError());

        error = gson.fromJson(Server.registerEmployer(gson.toJson(new RegisterEmployerDtoRequest("", "ptr20202", "123", "email@gmail.com", "asdfad", "sads"))), ErrorDtoResponse.class);
        assertEquals("WRONG_NAME", error.getError());
        error = gson.fromJson(Server.registerEmployer(gson.toJson(new RegisterEmployerDtoRequest("ptr20202", "", "123", "email@gmail.com", "asdfad", "sads"))), ErrorDtoResponse.class);
        assertEquals("WRONG_LOGIN", error.getError());
        error = gson.fromJson(Server.registerEmployer(gson.toJson(new RegisterEmployerDtoRequest("ывфывфы", "ptr20202", "", "email@gmail.com", "asdfad", "sads"))), ErrorDtoResponse.class);
        assertEquals("WRONG_PASSWORD", error.getError());
        error = gson.fromJson(Server.registerEmployer(gson.toJson(new RegisterEmployerDtoRequest("ывфывфы", "ptr20202", "dffsdfs", "", "asdfad", "sads"))), ErrorDtoResponse.class);
        assertEquals("WRONG_EMAIL", error.getError());
        error = gson.fromJson(Server.registerEmployer(gson.toJson(new RegisterEmployerDtoRequest("ывфывфы", "ptr20202", "dffsdfs", "sdf", "", "sads"))), ErrorDtoResponse.class);
        assertEquals("WRONG_COMPANY_NAME", error.getError());
        error = gson.fromJson(Server.registerEmployer(gson.toJson(new RegisterEmployerDtoRequest("ывфывфы", "ptr20202", "dffsdfs", "sdfsd", "asdfad", ""))), ErrorDtoResponse.class);
        assertEquals("WRONG_ADDRESS", error.getError());

        error = gson.fromJson(Server.registerEmployer(gson.toJson(new RegisterEmployerDtoRequest(null, "", "", "", "asdfad", "sads"))), ErrorDtoResponse.class);
        assertEquals("WRONG_NAME", error.getError());
        error = gson.fromJson(Server.registerEmployer(gson.toJson(new RegisterEmployerDtoRequest("ptr20202", null, " ", "", "asdfad", "sads"))), ErrorDtoResponse.class);
        assertEquals("WRONG_LOGIN", error.getError());
        error = gson.fromJson(Server.registerEmployer(gson.toJson(new RegisterEmployerDtoRequest("ывфывфы", "ptr20202", null, "", "asdfad", "sads"))), ErrorDtoResponse.class);
        assertEquals("WRONG_PASSWORD", error.getError());
        error = gson.fromJson(Server.registerEmployer(gson.toJson(new RegisterEmployerDtoRequest("ывфывфы", "ptr20202", "dffsdfs", null, "asdfad", "sads"))), ErrorDtoResponse.class);
        assertEquals("WRONG_EMAIL", error.getError());
        error = gson.fromJson(Server.registerEmployer(gson.toJson(new RegisterEmployerDtoRequest("ывфывфы", "ptr20202", "dffsdfs", "sdf", null, "sads"))), ErrorDtoResponse.class);
        assertEquals("WRONG_COMPANY_NAME", error.getError());
        error = gson.fromJson(Server.registerEmployer(gson.toJson(new RegisterEmployerDtoRequest("ывфывфы", "ptr20202", "dffsdfs", "sdfsd", "asdfad", null))), ErrorDtoResponse.class);
        assertEquals("WRONG_ADDRESS", error.getError());


    }

    @Test
    public void testChangeEmployer() {

        Gson gson = new Gson();
        RegisterEmployerDtoRequest employerDto = new RegisterEmployerDtoRequest("Ivanov55", "123", "ivanov", "ABCGame", "Address, 1/2", "email@gmail.com");
        String tokenEmployerDto = Server.registerEmployer(gson.toJson(employerDto));
        String tokenEmployer = gson.fromJson(tokenEmployerDto, TokenDtoRequest.class).getToken();
        ChangeEmployerDataRequest newEmployerData = new ChangeEmployerDataRequest(tokenEmployer, "ABC", "DEF", "0123", "XYZ", "GHJ");
        Server.changeEmployerData(gson.toJson(newEmployerData));

        Server.logOut(tokenEmployerDto);
        String newTokenDto = Server.logIn(gson.toJson(new LogInDtoRequest("123", "0123")));

        String string = Server.getEmployerData(newTokenDto);
        EmployerDataResponse employerData = gson.fromJson(string, EmployerDataResponse.class);
        assertEquals("ABC", employerData.getName());
        assertEquals("DEF", employerData.getEmail());
        assertEquals("GHJ", employerData.getAddress());
        assertEquals("XYZ", employerData.getCompanyName());

    }

    @Test
    public void testChangeEmployerWithException() {

        Gson gson = new Gson();
        RegisterEmployerDtoRequest employerDto = new RegisterEmployerDtoRequest("Ivanov55", "ivanov", "123", "email@gmail.com", "ABCGame", "Address, 1/2");
        String tokenEmployerDto = Server.registerEmployer(gson.toJson(employerDto));

        ErrorDtoResponse error = gson.fromJson(Server.changeEmployerData(gson.toJson(new ChangeEmployerDataRequest(tokenEmployerDto, "", "DEF", "0123", "XYZ", "GHJ"))), ErrorDtoResponse.class);
        assertEquals("WRONG_NAME", error.getError());
        error = gson.fromJson(Server.changeEmployerData(gson.toJson(new ChangeEmployerDataRequest(tokenEmployerDto, "dadsad", "", "0123", "XYZ", "GHJ"))), ErrorDtoResponse.class);
        assertEquals("WRONG_EMAIL", error.getError());
        error = gson.fromJson(Server.changeEmployerData(gson.toJson(new ChangeEmployerDataRequest(tokenEmployerDto, "asdasd", "DEF", "", "XYZ", "GHJ"))), ErrorDtoResponse.class);
        assertEquals("WRONG_PASSWORD", error.getError());
        error = gson.fromJson(Server.changeEmployerData(gson.toJson(new ChangeEmployerDataRequest(tokenEmployerDto, "asdada", "DEF", "0123", "", "GHJ"))), ErrorDtoResponse.class);
        assertEquals("WRONG_COMPANY_NAME", error.getError());
        error = gson.fromJson(Server.changeEmployerData(gson.toJson(new ChangeEmployerDataRequest(tokenEmployerDto, "sadfbgnh", "DEF", "0123", "XYZ", ""))), ErrorDtoResponse.class);
        assertEquals("WRONG_ADDRESS", error.getError());

        String string = Server.getEmployerData(tokenEmployerDto);
        EmployerDataResponse employerData = gson.fromJson(string, EmployerDataResponse.class);
        assertEquals("Ivanov55", employerData.getName());
        assertEquals("email@gmail.com", employerData.getEmail());
        assertEquals("ABCGame", employerData.getCompanyName());
        assertEquals("Address, 1/2", employerData.getAddress());


    }
}
