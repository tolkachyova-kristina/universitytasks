package net.thumbtack.school.hiring;

import com.google.gson.Gson;
import net.thumbtack.school.hiring.dto.request.employee.RegisterEmployeeDtoRequest;
import net.thumbtack.school.hiring.dto.request.employer.RegisterEmployerDtoRequest;
import net.thumbtack.school.hiring.dto.request.skill.AddNewSkillDtoRequest;
import net.thumbtack.school.hiring.dto.request.user.TokenDtoRequest;
import net.thumbtack.school.hiring.dto.request.vacancy.AddVacancyDtoRequest;
import net.thumbtack.school.hiring.dto.request.vacancy.VacancyServiceDtoRequest;
import net.thumbtack.school.hiring.dto.response.modelType.EmployeeDataDtoResponse;
import net.thumbtack.school.hiring.errors.ServerException;
import net.thumbtack.school.hiring.utils.Mode;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TestGetEmployeeList {
    final static Gson gson = new Gson();
    private static String employerToken;
    private static String tokenEmployeeA;
    private static String tokenEmployeeB;
    private static String tokenEmployeeC;
    private static String tokenEmployeeD;

    @BeforeAll
    public static void startTest() throws ServerException {
        Server.startServer("", Mode.SQL);
        RegisterEmployerDtoRequest employerDtoRequest = new RegisterEmployerDtoRequest("Ivanov55", "1234560", "ivanov", "ABCGame", "Address, 1/2", "email@gmail.com");
        employerToken = gson.fromJson(Server.registerEmployer(gson.toJson(employerDtoRequest)), TokenDtoRequest.class).getToken();
        AddVacancyDtoRequest newVacancy = new AddVacancyDtoRequest(employerToken, "Web Developer", 1500);
        newVacancy.addDemand("html", 3, true);
        newVacancy.addDemand("css", 3, false);
        newVacancy.addDemand("java-script", 5, true);
        newVacancy.addDemand("java", 5, false);
        Server.addVacancy(gson.toJson(newVacancy));

        RegisterEmployeeDtoRequest employeeDtoA = new RegisterEmployeeDtoRequest("A", "petr1", "1234", "1@mail.ru");
        tokenEmployeeA = gson.fromJson(Server.registerEmployee(gson.toJson(employeeDtoA)), TokenDtoRequest.class).getToken();
        Server.addNewSkill(gson.toJson(new AddNewSkillDtoRequest(tokenEmployeeA, "html", 3)));
        Server.addNewSkill(gson.toJson(new AddNewSkillDtoRequest(tokenEmployeeA, "css", 3)));
        Server.addNewSkill(gson.toJson(new AddNewSkillDtoRequest(tokenEmployeeA, "java-script", 5)));
        Server.addNewSkill(gson.toJson(new AddNewSkillDtoRequest(tokenEmployeeA, "java", 5)));

        RegisterEmployeeDtoRequest employeeDtoB = new RegisterEmployeeDtoRequest("B", "petr12", "1234", "2@mail.ru");
        tokenEmployeeB = gson.fromJson(Server.registerEmployee(gson.toJson(employeeDtoB)), TokenDtoRequest.class).getToken();
        Server.addNewSkill(gson.toJson(new AddNewSkillDtoRequest(tokenEmployeeB, "html", 3)));
        Server.addNewSkill(gson.toJson(new AddNewSkillDtoRequest(tokenEmployeeB, "css", 2)));
        Server.addNewSkill(gson.toJson(new AddNewSkillDtoRequest(tokenEmployeeB, "java-script", 5)));
        Server.addNewSkill(gson.toJson(new AddNewSkillDtoRequest(tokenEmployeeB, "java", 3)));


        RegisterEmployeeDtoRequest employeeDtoC = new RegisterEmployeeDtoRequest("C", "petr13", "1234", "3@mail.ru");
        tokenEmployeeC = gson.fromJson(Server.registerEmployee(gson.toJson(employeeDtoC)), TokenDtoRequest.class).getToken();
        Server.addNewSkill(gson.toJson(new AddNewSkillDtoRequest(tokenEmployeeC, "html", 3)));
        Server.addNewSkill(gson.toJson(new AddNewSkillDtoRequest(tokenEmployeeC, "css", 2)));
        Server.addNewSkill(gson.toJson(new AddNewSkillDtoRequest(tokenEmployeeC, "java-script", 1)));
        Server.addNewSkill(gson.toJson(new AddNewSkillDtoRequest(tokenEmployeeC, "java", 1)));


        RegisterEmployeeDtoRequest employeeDtoD = new RegisterEmployeeDtoRequest("D", "petr14", "1234", "4@mail.ru");
        tokenEmployeeD = gson.fromJson(Server.registerEmployee(gson.toJson(employeeDtoD)), TokenDtoRequest.class).getToken();
        Server.addNewSkill(gson.toJson(new AddNewSkillDtoRequest(tokenEmployeeD, "html", 5)));
    }

    @AfterAll
    public static void testStop() {
        Server.stopServer();
    }

    @Test
    public void testGetListOfEmployeesWhoHaveAllTheNecessarySkillsOnNeededLvl() {
        List<EmployeeDataDtoResponse> control = new ArrayList<>();
        control.add(gson.fromJson(Server.getEmployeeData(gson.toJson(new TokenDtoRequest(tokenEmployeeA))), EmployeeDataDtoResponse.class));
        assertEquals(control.toString().replaceAll("\\s", ""), Server.getListOfEmployeesWhoHaveAllTheNecessarySkillsOnNeededLvl(gson.toJson(new VacancyServiceDtoRequest(employerToken, "Web Developer"))));
    }

    @Test
    public void testGetListOfEmployeesWhoHaveAllTheRequiredSkillsOnNeededLvl() {
        List<String> control = new ArrayList<>();
        control.add(Server.getEmployeeData(gson.toJson(new TokenDtoRequest(tokenEmployeeA))));
        control.add(Server.getEmployeeData(gson.toJson(new TokenDtoRequest(tokenEmployeeB))));
        assertEquals(control.toString().replaceAll("\\s", ""), Server.getListOfEmployeesWhoHaveAllTheRequiredSkillsOnNeededLvl(gson.toJson(new VacancyServiceDtoRequest(employerToken, "Web Developer"))));
    }

    @Test
    public void testGetListOfEmployeesWhoHaveAllTheNecessarySkillsOnAnyLvl() {
        List<EmployeeDataDtoResponse> control = new ArrayList<>();

        control.add(gson.fromJson(Server.getEmployeeData(gson.toJson(new TokenDtoRequest(tokenEmployeeA))), EmployeeDataDtoResponse.class));
        control.add(gson.fromJson(Server.getEmployeeData(gson.toJson(new TokenDtoRequest(tokenEmployeeB))), EmployeeDataDtoResponse.class));
        control.add(gson.fromJson(Server.getEmployeeData(gson.toJson(new TokenDtoRequest(tokenEmployeeC))), EmployeeDataDtoResponse.class));
        assertEquals(control.toString().replaceAll("\\s", ""), Server.getListOfEmployeesWhoHaveAllTheNecessarySkillsOnAnyLvl(gson.toJson(new VacancyServiceDtoRequest(employerToken, "Web Developer"))));

    }

    @Test
    public void testGetListOfEmployeesWhoHaveAtLeastOneTheNecessarySkillsOnAnyLvl() {
        List<EmployeeDataDtoResponse> control = new ArrayList<>();
        control.add(gson.fromJson(Server.getEmployeeData(gson.toJson(new TokenDtoRequest(tokenEmployeeA))), EmployeeDataDtoResponse.class));
        control.add(gson.fromJson(Server.getEmployeeData(gson.toJson(new TokenDtoRequest(tokenEmployeeB))), EmployeeDataDtoResponse.class));
        control.add(gson.fromJson(Server.getEmployeeData(gson.toJson(new TokenDtoRequest(tokenEmployeeC))), EmployeeDataDtoResponse.class));
        control.add(gson.fromJson(Server.getEmployeeData(gson.toJson(new TokenDtoRequest(tokenEmployeeD))), EmployeeDataDtoResponse.class));

        assertEquals(control.toString().replaceAll("\\s", ""), Server.getListOfEmployeesWhoHaveAtLeastOneTheNecessarySkillsOnAnyLvl(gson.toJson(new VacancyServiceDtoRequest(employerToken, "Web Developer"))));

    }
}
